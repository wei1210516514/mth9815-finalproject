Project Description:
* MTH 9815 Final Project -- Trading System
* Instructor: Breman Thuraisingham 
* Model the infrastructure of a bond trading system by SOA using C++


Ubuntu Code (after getting into the directory)
*  g++ -m64 -g main.cpp -o main -lboost_date_time -lboost_system   (in one screen)
*  ./main
*  
*  g++ -m64 -g Client.cpp -o main -lboost_date_time -lboost_system  (in another screen)
*  ./main
*   
* (Attention: in visual studio use Sleep(300), and in Ubuntu use usleep(300000))


Workflows:
* Workflow 1: trades.txt->TCPIP->BondTradeBookingService->BondPositionService->BondRiskService
* Workflow 2: marketdata.txt->TCPIP->BondMarketDataService->BondAlgoExecutionService->BondExecutionService->BondTradeBookingService
* Workflow 3: price.txt->TCPIP->BondAlgoStreamingService->BondStreamingService
* Workflow 4: inquiry.txt->TCPIP->BondInquiryService->BondInquiryService
* Workflow 5: 
*             BondPositionService->BondPositionHistoricalDataService->positions.txt
*             BondRiskService->BondRiskHistoricalDataService->risk.txt
*             BondExecutionService->BondExecutionHistoricalDataService->execution.txt
*             BondStreamingService->BondStreamingHistoricalDataService->streaming.txt
*             BondInquiryService->BondInquiryHistoricalDataService->inquiry.txt

	
Modifications to the original service codes:
* executionservice.hpp: 
	* add an empty default ctor in the ExecutionOrder<T> class
* inquiryservice.hpp: 			
	* add an empty default ctor in the Inquiry<T> class
	* add SetState in the Inquiry<T> class
* marketdataservice.hpp:
	* add an empty default ctor in the OrderBook<T> class
	* add a GetBestBidOffer() function in the OrderBook<T> class to get the best bid-offer order pair within this orderbook
* positionservice.hpp:
	* add an empty default ctor in the Position<T> class
	* implement the position_summation() function in the Position<T> class
* pricingservice.hpp:
	* add an empty default ctor in the Price<T> class
* riskservice.hpp
	* add an empty default ctor in the PV01<T> class and the BucketedSector<T> class
	* implement the GetProduct(), GetPV01() and GetQuantity() functions in the PV01<T> class
	* add 'virtual' keyword to the AddPosition() and GetBucketedRisk() functions in the RiskService<T> class
* streamingservice.hpp:
	* add an empty default ctor in the PriceStreamOrder<T> class and the PriceStream<T> class
	* add 'virtual' keyword to the PublishPrice() function in the StreamingService<T> class
* tradebookingservice.hpp:
	* add an empty default ctor in the Trade<T> class
	* add 'virtual' keyword to the BookTrade() function in the TradeBookingService<T> class
	
Contact Information:
* Arthor: Mingjie (Jonathan) Wei
* Email: weimingjie1017@gmail.com