/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include "soa.hpp"
#include "tradebookingservice.hpp"
using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template<typename T>
class Position
{

public:

  // ctor for a position
	Position() {};
	Position(const T &_product);

	// Get the product
	const T& GetProduct() const;

	// Get the position quantity
	long GetPosition(string &book);

	// Get the aggregate position
	long GetAggregatePosition();

	//add new positions
	void position_summation(string bookNum, int quantity);

private:
	T product;
	map<string,long> positions;  //book,quantity
};

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PositionService : public Service<string,Position <T> >
{

public:

  // Add a trade to the service
  virtual void AddTrade(const Trade<T> &trade) = 0;

};

template<typename T>
Position<T>::Position(const T &_product) :
  product(_product)
{
}

template<typename T>
const T& Position<T>::GetProduct() const
{
  return product;
}

template<typename T>
long Position<T>::GetPosition(string &book)
{
	if (positions.find(book) != positions.end())
		return positions[book];
	else return 0;
}

template<typename T>
long Position<T>::GetAggregatePosition()
{
	long long result = 0;
	for (auto it = positions.begin(); it != positions.end(); ++it)
		result += it->second;
	return result;
}

//add new positions
template<typename T>
void Position<T>::position_summation(string bookNum, int quantity)
{
	if (positions.find(bookNum) != positions.end())
		positions[bookNum] += quantity;
	else
		positions[bookNum] = quantity;
}

#endif
