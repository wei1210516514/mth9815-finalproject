#ifndef OUTPUTDATAGENERATION_HPP
#define OUTPUTDATAGENERATION_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <random>
#include <vector>
#include "bondpositionservice.h"
#include "bondriskservice.h"
#include "bondexecutionservice.h"
#include "bondstreamingservice.h"
#include "bondinquiryservice.h"
using namespace std;

void PositionOutput(Position<Bond> data)
{
	/*
		position for given book as well as the aggregate position
	*/
	ofstream outputfile;
	outputfile.open("positions.txt",ios::app);
	string timepoint = CurrentTime(false);
	outputfile << timepoint << ",CUSIP," << data.GetProduct().GetProductId() << ",";
	string book = "TRSY1";
	outputfile << book << "," << to_string(data.GetPosition(book)) <<",";
	book = "TRSY2";
	outputfile << book << "," << to_string(data.GetPosition(book)) << ",";
	book = "TRSY3";
	outputfile << book << "," << to_string(data.GetPosition(book)) << ",";
	outputfile << "AGGREGATE," << to_string(data.GetAggregatePosition()) << endl;
}


void RiskBondOutput(PV01<Bond> data)
{	
	/*
		risk for each security
	*/
	ofstream outputfile;
	outputfile.open("risk.txt",ios::app);
	string timepoint = CurrentTime(false);
	outputfile << timepoint << ",CUSIP," << data.GetProduct().GetProductId() <<"," <<
		to_string(data.GetPV01()) << "," << to_string(data.GetQuantity()) << endl;
}

void RiskBucketedOutput(PV01<BucketedSector<Bond> > data)
{	/*
		risk for bucketed sector
	*/
	ofstream outputfile;
	outputfile.open("risk.txt",ios::app);
	string timepoint = CurrentTime(false);
	outputfile << timepoint << ",BUCKETED_SECTOR," << data.GetProduct().GetName() << "," <<
		to_string(data.GetPV01()) << "," << to_string(data.GetQuantity()) << endl;
}

void ExecutionOutput(ExecutionOrder<Bond> data)
{
	ofstream outputfile;
	outputfile.open("executions.txt",ios::app);
	string timepoint = CurrentTime(false),type,side,childorder;
	OrderType ot = data.GetOrderType();
	if (ot == FOK) type = "FOK";
	else if (ot == IOC) type = "IOC";
	else if (ot == MARKET) type = "MARKET";
	else if (ot == LIMIT) type = "LIMIT";
	else if (ot == STOP) type = "STOP";
	if (data.GetPricingSide() == BID) side = "BID";
	else side = "OFFER";
	if (data.IsChildOrder()) childorder = "TRUE";
	else childorder = "FALSE";
	outputfile << timepoint << "," << type << "," << data.GetOrderId() << ",CUSIP," 
		<< data.GetProduct().GetProductId() << ","
		<< side << "," << std::to_string(data.GetVisibleQuantity()) << ","
		<< std::to_string(data.GetHiddenQuantity()) << "," << Double2String(data.GetPrice()) << ","
		<< childorder << "," << data.GetParentOrderId() << endl;
}

void StreamingOutput(PriceStream<Bond> data)
{
	ofstream outputfile;
	outputfile.open("streaming.txt",ios::app);
	string timepoint = CurrentTime(false);
	outputfile << timepoint << ",CUSIP," << data.GetProduct().GetProductId() << ","
		<< std::to_string(data.GetBidOrder().GetPrice()) << ","
		<< std::to_string(data.GetBidOrder().GetVisibleQuantity()) << ","
		<< std::to_string(data.GetBidOrder().GetHiddenQuantity()) << ","
		<< std::to_string(data.GetOfferOrder().GetPrice()) << ","
		<< std::to_string(data.GetBidOrder().GetVisibleQuantity()) << ","
		<< std::to_string(data.GetBidOrder().GetHiddenQuantity()) << endl;
}

void InquiryOutput(Inquiry<Bond> data)
{
	ofstream outputfile;
	outputfile.open("allinquiries.txt",ios::app);
	string timepoint = CurrentTime(false), side,state;
	InquiryState temp = data.GetState();
	if (data.GetSide() == BUY) side = "BUY";
	else side = "SELL";
	if (temp == QUOTED) state = "QUOTED";
	else if (temp == DONE) state = "DONE";
	else if (temp == REJECTED) state = "REJECTED";
	else if (temp == CUSTOMER_REJECTED) state = "CUSTOMER_REJECTED";

	outputfile << timepoint <<"," << data.GetInquiryId() << ",CUSIP,"
		<< data.GetProduct().GetProductId() << "," << data.GetProduct().GetTicker() << ","
		<< std::to_string(data.GetQuantity()) << "," << Double2String(data.GetPrice()) << "," << state << endl;
}

#endif
