#ifndef BONDPOSITIONHISTORICALDATASERVICE_HPP
#define BONDPOSITIONHISTORICALDATASERVICE_HPP
/*
	BondPositionService -> HistoricalDataService->positions.txt
	BondRiskService -> HistoricalDataService->risk.txt
	BondExecutionService -> HistoricalDataService->executions.txt
	BondStreamingService -> HistoricalDataService->streaming.txt
	BondInquiryService -> HistoricalDataService->inquiry.txt
*/

#include <vector>
#include <map>
#include "outputdatageneration.h"
using namespace std;

class BondPositionHistoricalDataConnector;

class BondPositionHistoricalDataService : public Service< string, Position<Bond> >
{
private:
	vector<ServiceListener<Position<Bond> >*> Listeners;
	BondPositionHistoricalDataConnector* bondTConnector;
	map<string, Position<Bond> > bondT;
public:
	BondPositionHistoricalDataService();

	// Get data on our service given a key
	virtual Position<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Position<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Position<Bond> >* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<Position<Bond> >* >& GetListeners() const override;

	BondPositionHistoricalDataConnector* GetConnector() const;

	// Persist data to a store
	void PersistData(string persistKey, const Position<Bond>& data);
};

class BondPositionHistoricalDataConnector : public Connector< Position<Bond> >
{
private:
	BondPositionHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondPositionHistoricalDataConnector(BondPositionHistoricalDataService* HDS);

	// Publish data to the Connector
	void Publish(Position<Bond>& data);
};

BondPositionHistoricalDataConnector::BondPositionHistoricalDataConnector(BondPositionHistoricalDataService* HDS) :
	bondService(HDS) {}

void BondPositionHistoricalDataConnector::Publish(Position<Bond>& data)
{
	PositionOutput(data);
}

BondPositionHistoricalDataService::BondPositionHistoricalDataService()
{
	bondTConnector = new BondPositionHistoricalDataConnector(this);
}


Position<Bond>& BondPositionHistoricalDataService::GetData(string key)
{
	return bondT[key];
}

void BondPositionHistoricalDataService::OnMessage(Position<Bond>& data) {}

void BondPositionHistoricalDataService::AddListener(ServiceListener<Position<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<Position<Bond> >* >& BondPositionHistoricalDataService::GetListeners() const
{
	return Listeners;
}

BondPositionHistoricalDataConnector* BondPositionHistoricalDataService::GetConnector() const
{
	return bondTConnector;
}

void BondPositionHistoricalDataService::PersistData(string persistKey, const Position<Bond>& data)
{
	bondT[persistKey] = data;
	Position<Bond> temp = data;
	bondTConnector->Publish(temp);
}

//listener from higher-level operations
class BondPositionHistoricalDataServiceListener : public ServiceListener<Position<Bond> >
{
private:
	BondPositionHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondPositionHistoricalDataServiceListener(BondPositionHistoricalDataService* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(Position<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(Position<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(Position<Bond>& data);
};

BondPositionHistoricalDataServiceListener::BondPositionHistoricalDataServiceListener(BondPositionHistoricalDataService* HDSL) :
	bondService(HDSL) {}

void BondPositionHistoricalDataServiceListener::ProcessAdd(Position<Bond>& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);
}

void BondPositionHistoricalDataServiceListener::ProcessRemove(Position<Bond>& data) {}

void BondPositionHistoricalDataServiceListener::ProcessUpdate(Position<Bond>& data) {}



#endif
