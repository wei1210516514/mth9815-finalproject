#ifndef BONDSTREAMINGHISTORICALDATASERVICE_HPP
#define BONDSTREAMINGHISTORICALDATASERVICE_HPP

/*
	BondStreamingService -> HistoricalDataService->streaming.txt
*/

#include <vector>
#include <map>
#include "UtilityFunctions.h"
#include "outputdatageneration.h"
using namespace std;

class BondStreamingHistoricalDataConnector;

class BondStreamingHistoricalDataService : public Service<string, PriceStream<Bond> >
{
private:
	vector<ServiceListener<PriceStream<Bond> >*> Listeners;
	BondStreamingHistoricalDataConnector* bondTConnector;
	map<string, PriceStream<Bond> > bondT;
public:
	BondStreamingHistoricalDataService();

	// Get data on our service given a key
	virtual PriceStream<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(PriceStream<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PriceStream<Bond>>* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<PriceStream<Bond> >* >& GetListeners() const override;

	BondStreamingHistoricalDataConnector* GetConnector() const;

	// Persist data to a store
	void PersistData(string persistKey, const PriceStream<Bond>& data);
};

class BondStreamingHistoricalDataConnector : public Connector<PriceStream<Bond> >
{
private:
	BondStreamingHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondStreamingHistoricalDataConnector(BondStreamingHistoricalDataService* HDS);

	// Publish data to the Connector
	void Publish(PriceStream<Bond>& data);
};


BondStreamingHistoricalDataConnector::BondStreamingHistoricalDataConnector(BondStreamingHistoricalDataService* HDS) :
	bondService(HDS) {}

void BondStreamingHistoricalDataConnector::Publish(PriceStream<Bond>& data)
{
	StreamingOutput(data);
}

BondStreamingHistoricalDataService::BondStreamingHistoricalDataService()
{
	bondTConnector = new BondStreamingHistoricalDataConnector(this);
}


PriceStream<Bond>& BondStreamingHistoricalDataService::GetData(string key)
{
	return bondT[key];
}

void BondStreamingHistoricalDataService::OnMessage(PriceStream<Bond>& data) {}


void BondStreamingHistoricalDataService::AddListener(ServiceListener<PriceStream<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<PriceStream<Bond> >* >& BondStreamingHistoricalDataService::GetListeners() const
{
	return Listeners;
}

BondStreamingHistoricalDataConnector* BondStreamingHistoricalDataService::GetConnector() const
{
	return bondTConnector;
}

void BondStreamingHistoricalDataService::PersistData(string persistKey, const PriceStream<Bond>& data)
{
	bondT[persistKey] = data;
	PriceStream<Bond> temp = data;
	bondTConnector->Publish(temp);
}

//listener from higher-level operations
class BondStreamingHistoricalDataServiceListener : public ServiceListener<PriceStream<Bond> >
{
private:
	BondStreamingHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondStreamingHistoricalDataServiceListener(BondStreamingHistoricalDataService* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(PriceStream<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(PriceStream<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(PriceStream<Bond>& data);
};


BondStreamingHistoricalDataServiceListener::BondStreamingHistoricalDataServiceListener(BondStreamingHistoricalDataService* HDSL) :
	bondService(HDSL) {}

void BondStreamingHistoricalDataServiceListener::ProcessAdd(PriceStream<Bond>& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);
}

void BondStreamingHistoricalDataServiceListener::ProcessRemove(PriceStream<Bond>& data) {}
void BondStreamingHistoricalDataServiceListener::ProcessUpdate(PriceStream<Bond>& data) {}

#endif