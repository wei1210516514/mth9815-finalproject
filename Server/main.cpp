#include "bondalgoexecutionservice.h"
#include "bondalgostreamingservice.h"
#include "bondguiservice.h"
#include "bondmarketdataservice.h"
#include "bondpricingservice.h"
#include "bondtradebookingservice.h"
#include "bondinquiryhistoricaldataservice.h"
#include "bondexecutionhistoricaldataservice.h"
#include "bondpositionhistoricaldataservice.h"
#include "bondstreaminghistoricaldataservice.h"
#include "bondriskhistoricaldataservice.h"
using namespace std;

int main()
{
	/*
		//data generation
		trade_data_generate();
		price_data_generate();
		marketdata_generate();
		inquiry_generate();
	*/

	//trade to position and risk
	cout << CurrentTime(false) << " Workflow 1 Program starts and links: " << endl;
	BondTradeBookingService bondTradeBookingService;
	BondPositionService bondPositionService;
	BondPositionServiceListener bondPositionListener(&bondPositionService);

	BondRiskService bondRiskService;
	BondRiskServiceListener bondRiskListener(&bondRiskService);

	BondRiskHistoricalDataService bondRiskHistoricalDataService;
	BondRiskHistoricalDataServiceListener bondRiskHistoricalDataListener(&bondRiskHistoricalDataService);
	BondRiskHistoricalDataConnector bondRiskHistoricalDataConnector(&bondRiskHistoricalDataService);

	BondPositionHistoricalDataService bondPositionHistoricalDataService;
	BondPositionHistoricalDataConnector bondPositionHistoricalDataConnector(&bondPositionHistoricalDataService);
	BondPositionHistoricalDataServiceListener bondPositionHistoricalDataListener(&bondPositionHistoricalDataService);

	bondTradeBookingService.AddListener(&bondPositionListener);
	bondPositionService.AddListener(&bondRiskListener);
	bondPositionService.AddListener(&bondPositionHistoricalDataListener);
	bondRiskService.AddListener(&bondRiskHistoricalDataListener);

	//marketdata to execution
	cout << CurrentTime(false) << " Workflow 2 Program starts and links: " << endl;
	MarketDataService bondMarketDataService;
	BondAlgoExecutionService bondAlgoExecutionService;
	BondAlgoExecutionServiceListener bondAlgoExecutionListener(&bondAlgoExecutionService);
	BondExecutionService bondExecutionService;
	BondExecutionServiceListener bondExecutionListener(&bondExecutionService);
	BondTradeServiceListener bondTradeBookingListener(&bondTradeBookingService);
	BondExecutionHistoricalDataService bondExecutionHistoricalDataService;
	BondExecutionHistoricalDataConnector bondExecutionHistoricalDataConnector(&bondExecutionHistoricalDataService);
	BondExecutionHistoricalDataServiceListener bondExecutionHistoricalDataListener(&bondExecutionHistoricalDataService);

	bondMarketDataService.AddListener(&bondAlgoExecutionListener);
	bondAlgoExecutionService.AddListener(&bondExecutionListener);
	bondExecutionService.AddListener(&bondTradeBookingListener);
	bondExecutionService.AddListener(&bondExecutionHistoricalDataListener);

	//prices to gui and streaming
	cout << CurrentTime(false) << " Workflow 3 Program starts and links: " << endl;
	BondPricingService pricingService;
	BondAlgoStreamingService algoStreamingService;
	BondAlgoStreamingServiceListener algoStreamingServiceListener(&algoStreamingService);
	BondStreamingService streamingService;
	BondStreamingServiceListener streamingServiceListener(&streamingService);
	BondStreamingHistoricalDataService bondStreamingHistoricalDataService;
	BondStreamingHistoricalDataConnector bondStreamingHistoricalDataConnector(&bondStreamingHistoricalDataService);
	BondStreamingHistoricalDataServiceListener bondStreamingHistoricalDataListener(&bondStreamingHistoricalDataService);
	BondGUIService bondGUIService;
	BondGUIConnector bondGUIConnector(&bondGUIService);
	BondGUIServiceListener bondGUIServiceListener(&bondGUIService);

	pricingService.AddListener(&algoStreamingServiceListener);
	algoStreamingService.AddListener(&streamingServiceListener);
	streamingService.AddListener(&bondStreamingHistoricalDataListener);
	pricingService.AddListener(&bondGUIServiceListener);

	//inquiry
	cout << CurrentTime(false) << " Workflow 4 Program starts and links: " << endl;
	BondInquiryService bondInquiryService;
	BondInquiryServiceListener bondInquiryListener(&bondInquiryService);
	BondInquiryHistoricalDataService bondInquiryHistoricalDataService;
	BondInquiryHistoricalDataConnector bondInquiryHistoricalDataConnector(&bondInquiryHistoricalDataService);
	BondInquiryHistoricalDataServiceListener bondInquiryHistoricalDataListener(&bondInquiryHistoricalDataService);

	cout << CurrentTime(false) << " Start subscribing documents." << endl;
	// link the service components
	bondInquiryService.AddListener(&bondInquiryHistoricalDataListener);
	bondInquiryService.AddListener(&bondInquiryListener);

	cout << CurrentTime(false) << " trades..." << endl;
	BondTradeConnector bondTradeBookingConnector(&bondTradeBookingService);
	bondTradeBookingConnector.Subscribe();

	cout << CurrentTime(false) << " marketdata..." << endl;
	MarketDataConnector bondMarketDataConnector(&bondMarketDataService);
	bondMarketDataConnector.Subscribe();

	cout << CurrentTime(false) << " prices..." << endl;
	BondPricingConnector pricingConnector(&pricingService);
	pricingConnector.Subscribe();

	cout << CurrentTime(false) << " inquiry..." << endl;
	BondInquiryConnector bondInquiryConnector(&bondInquiryService);
	bondInquiryConnector.Subscribe();
	cout << CurrentTime(false) << " The process finished." << endl;

	return 0;
}