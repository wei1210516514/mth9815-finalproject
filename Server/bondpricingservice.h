#ifndef BONDPRICINGSERVICE_HPP
#define BONDPRICINGSERVICE_HPP

/*
	Bond pricing service
	Derive:
	bondpricingservice -> service
	bondpricingconnector -> connector
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "soa.hpp"
#include "pricingservice.hpp"
#include "UtilityFunctions.h"

using namespace std;

class BondPricingService : public Service< string, Price<Bond> >
{
private:
	map< string, Price<Bond> > prices;
	vector< ServiceListener< Price<Bond> >* > Listeners;
public:
	BondPricingService() {};
	virtual void OnMessage(Price<Bond>& data) override;
	// Get data on our service given a key
	virtual Price<Bond>& GetData(string key) override;
	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Price<Bond> >* listener);

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Price<Bond> >* >& GetListeners() const;
};

Price<Bond>& BondPricingService::GetData(string key)
{
	return prices[key];
}

void BondPricingService::OnMessage(Price<Bond>& data)
{
	prices.insert(pair< string, Price<Bond> >(data.GetProduct().GetProductId(),data));
	for (int i = 0; i < Listeners.size(); ++i)
	{
		Listeners[i]->ProcessAdd(data);
	}
}

void BondPricingService::AddListener(ServiceListener<Price<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<Price<Bond> >* >& BondPricingService::GetListeners() const
{
	return Listeners;
}

class BondPricingConnector : public Connector< Price<Bond> >
{
private:
	BondPricingService* bondService;
public:
	BondPricingConnector(BondPricingService* BPS);
	void Publish(Price<Bond>& data) override;
	void Subscribe(); 
};

BondPricingConnector::BondPricingConnector(BondPricingService* BPS) :bondService(BPS) {};

void BondPricingConnector::Publish(Price<Bond>& data) {};

void BondPricingConnector::Subscribe()
{
	boost::asio::io_service io_service;
	//listen for new connection
	tcp::acceptor acceptor_(io_service, tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 2345));
	//socket creation 
	tcp::socket socket_(io_service);
	//waiting for connection
	acceptor_.accept(socket_);

	string newBond;
	while (true)
	{
		string temp = read_(socket_);
		if (temp == "...") break;
		cout << temp << endl;
		stringstream newStream(temp);
		vector<string> tmp;
		while (getline(newStream, temp, ','))
		{
			tmp.push_back(temp);
		}
		string _productId_ = tmp[0]; 
		double _bidprice_ = String2Double(tmp[1]), _askprice_ = String2Double(tmp[2].substr(0,tmp[2].size()-1));
		double _midprice_ = (_bidprice_ + _askprice_) / 2, spread = _askprice_ - _bidprice_;
		Bond _product_ = getBond(_productId_);
		Price<Bond> newSecurity(_product_, _midprice_, spread);
		bondService->OnMessage(newSecurity);
	}
	send_(socket_, "Prices data finish transmission!");
}
#endif
