#ifndef BONDINQUIRYSERVICE_HPP
#define BONDINQUIRYSERVICE_HPP

/*
	Bond inquiry service
	Derive:
	bondinquiryservicelistener -> servicelistener
	bondinquiryservice -> service
	bondinquiryconnector -> connector
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>
#include <unordered_map>
#include "inquiryservice.hpp"
#include "products.hpp"
#include "UtilityFunctions.h"

/**
 * Service for customer inquirry objects.
 * Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
 * Type T is the product type.
 */

class BondInquiryConnector;

class BondInquiryService :public Service< string, Inquiry<Bond> >
{
private:
	map< string, Inquiry<Bond> > bondinquiry;
	vector<ServiceListener<Inquiry<Bond> >*> Listeners;
	BondInquiryConnector* bondinquiryconnector;
public:
	BondInquiryService();

	// Get data on our service given a key
	virtual Inquiry<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Inquiry<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Inquiry<Bond>>* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Inquiry<Bond>>* >& GetListeners() const override;

	// Send a quote back to the client
	virtual void SendQuote(const string& inquiryId, double price);

	// Reject an inquiry from the client
	virtual void RejectInquiry(const string& inquiryId);

	// Get the connector of the service
	BondInquiryConnector* GetConnector();
};

class BondInquiryConnector :public Connector< Inquiry<Bond> >
{
private:
	BondInquiryService* bondService;
public:
	BondInquiryConnector(BondInquiryService* BIS);

	// Publish data to the Connector
	void Publish(Inquiry<Bond>& data);

	// Subscribe data from the Connector
	void Subscribe();

	// Re-subscribe data from the Connector
	void Subscribe(Inquiry<Bond>& data);
};

BondInquiryService::BondInquiryService()
{
	bondinquiryconnector = new BondInquiryConnector(this);
}

Inquiry<Bond>& BondInquiryService::GetData(string key)
{
	return bondinquiry[key];
}

void BondInquiryService::OnMessage(Inquiry<Bond>& data)
{
	bondinquiry[data.GetProduct().GetProductId()] = data;
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(data);
}

void BondInquiryService::AddListener(ServiceListener<Inquiry<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<Inquiry<Bond>>* >& BondInquiryService::GetListeners() const
{
	return Listeners;
}

BondInquiryConnector* BondInquiryService::GetConnector()
{
	return bondinquiryconnector;
}

void BondInquiryService::SendQuote(const string& inquiryId, double price)
{
	Inquiry<Bond> temp = bondinquiry[inquiryId];
	Inquiry<Bond> newTemp(inquiryId, temp.GetProduct(), temp.GetSide(), temp.GetQuantity(), price, temp.GetState());
	bondinquiryconnector->Publish(newTemp);
}

void BondInquiryService::RejectInquiry(const string& inquiryId)
{
	Inquiry<Bond> temp = bondinquiry[inquiryId];
	Inquiry<Bond> newTemp(inquiryId, temp.GetProduct(), temp.GetSide(), temp.GetQuantity(), temp.GetPrice(),REJECTED);
	bondinquiryconnector->Publish(newTemp);
}

BondInquiryConnector::BondInquiryConnector(BondInquiryService* BIS) :
	bondService(BIS) {}

void BondInquiryConnector::Publish(Inquiry<Bond>& data)
{
	if (data.GetState() == REJECTED)
		bondService->OnMessage(data);
	else if (data.GetState() == RECEIVED)
	{
		data.SetState(QUOTED);
		bondService->OnMessage(data);
		data.SetState(DONE);
		bondService->OnMessage(data);
	}
}

void BondInquiryConnector::Subscribe(Inquiry<Bond>& data)
{
	bondService->OnMessage(data);
}

void BondInquiryConnector::Subscribe()
{
	boost::asio::io_service io_service;
	//listen for new connection
	tcp::acceptor acceptor_(io_service, tcp::endpoint(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 1234)));
	//socket creation 
	tcp::socket socket_(io_service);
	//waiting for connection
	acceptor_.accept(socket_);

	string newinquiry;
	while (true)
	{
		string tmp = read_(socket_);
		if (tmp == "...") break;
		cout << tmp << endl;
		stringstream st(tmp);
		vector<string> temp;
		while (getline(st, tmp, ','))
		{
			temp.push_back(tmp);
		}
		//string _inquiryId
		string inquiryId = temp[0];

		//const T &_product
		string productID = temp[1];
		Bond product = getBond(productID);

		//Side _side
		Side side;
		if (temp[2] == "BUY") side = BUY;
		else side = SELL;

		//long _quantity
		long quantity = stoi(temp[3]);

		//double _price
		double price = String2Double(temp[4]);

		//InquiryState _state
		InquiryState state;
		if (temp[5] == "RECEIVED") state = RECEIVED;
		else if (temp[5] == "QUOTED") state = QUOTED;
		else if (temp[5] == "DONE") state = DONE;
		else if (temp[5] == "REJECTED") state = REJECTED;
		else if (temp[5] == "CUSTOMER_REJECTED") state = CUSTOMER_REJECTED;

		Inquiry<Bond> newInquiry(inquiryId, product, side, quantity, price, state);
		bondService->OnMessage(newInquiry);
	}
	send_(socket_, "Inquiry data finish transmission!");
}


//listener from inquiry itself
class BondInquiryServiceListener : public ServiceListener<Inquiry<Bond> >
{
private:
	BondInquiryService* bondService;
public:
	BondInquiryServiceListener(BondInquiryService* BIS);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Inquiry<Bond>& data) override;

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Inquiry<Bond>& data) override;

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Inquiry<Bond>& data) override;
};

BondInquiryServiceListener::BondInquiryServiceListener(BondInquiryService* BIS):
	bondService(BIS){}

void BondInquiryServiceListener::ProcessAdd(Inquiry<Bond>& data)
{
	if (data.GetState() == RECEIVED)
		bondService->SendQuote(data.GetInquiryId(), 100.0);
}

void BondInquiryServiceListener::ProcessRemove(Inquiry<Bond>& data) {}
void BondInquiryServiceListener::ProcessUpdate(Inquiry<Bond>& data) {}

#endif
