#ifndef BONDTRADEBOOKINGSERVICE_HPP
#define BONDTRADEBOOKINGSERVICE_HPP
/*
	Bond trade booking service
	Derive:
	bondtradeservicelistener -> servicelistener
	tradebookingservice -> service
	bondtradeconnector -> connector
*/

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <tuple>
#include "products.hpp"
#include "soa.hpp"
#include "tradebookingservice.hpp"
#include "executionservice.hpp"
#include "UtilityFunctions.h"
#include <boost/asio.hpp>
using namespace std;
using namespace boost::asio;
using ip::tcp;

/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on trade id.
 * Type T is the product type.
 */
class BondTradeBookingService : public Service<string, Trade <Bond> >
{
private:
	map< string, Trade<Bond> > trades;
	vector< ServiceListener< Trade<Bond> >* > Listeners;
public:
	BondTradeBookingService();

	// Book the trade
	virtual void BookTrade(const Trade<Bond>& trade);

	// Get data on our service given a key
	virtual Trade<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Trade<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener< Trade<Bond> >* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener< Trade<Bond> >* >& GetListeners() const override;
};

BondTradeBookingService::BondTradeBookingService() {};

Trade<Bond>& BondTradeBookingService::GetData(string key)
{
	return trades[key];
}

void BondTradeBookingService::OnMessage(Trade<Bond>& data)
{
	BookTrade(data);
}

void BondTradeBookingService::AddListener(ServiceListener< Trade<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener< Trade<Bond> >* >& BondTradeBookingService::GetListeners() const
{
	return Listeners;
}

void BondTradeBookingService::BookTrade(const Trade<Bond>& trade)
{
	trades[trade.GetTradeId()] = trade;
	Trade<Bond> temp = trade;
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(temp);
}


//Bond Trade Connector
class BondTradeConnector :public Connector< Trade<Bond> >
{
private:
	BondTradeBookingService* bondService;
public:
	BondTradeConnector(BondTradeBookingService* BPS);
	void Publish(Trade<Bond>& data) override;
	void Subscribe(); //tcp::endpoint ep;
};

BondTradeConnector::BondTradeConnector(BondTradeBookingService* BPS):bondService(BPS){};

void BondTradeConnector::Publish(Trade<Bond>& data) {};

void BondTradeConnector::Subscribe() 
{  
	boost::asio::io_service io_service;
	//listen for new connection
	tcp::acceptor acceptor_(io_service, tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 3456));
	//socket creation 
	tcp::socket socket_(io_service);
	//waiting for connection
	acceptor_.accept(socket_);

	string newBond;
	while (true)
	{
		string temp = read_(socket_);
		if (temp == "...") break;
		cout << temp << endl;
		stringstream newStream(temp);
		vector<string> tmp;
		while (getline(newStream, temp, ',')) tmp.push_back(temp);
		string _productId_ = tmp[0];
		string _tradeId_ = tmp[1];
		double _price_ = String2Double(tmp[2]);
		string _book_ = tmp[3];
		long _quantity_ = stoi(tmp[4]);
		Side _side_;
		if (tmp[5] == "BUY") _side_ = BUY;
		else _side_ = SELL;
		Bond _product_ = getBond(_productId_);
		Trade<Bond> newSecurity(_product_, _tradeId_, _price_, _book_, _quantity_, _side_);
		bondService->OnMessage(newSecurity);
	}
	send_(socket_, "Trades data finish transmission!");
}


//Bond Trade Listener from execution
class BondTradeServiceListener : public ServiceListener< ExecutionOrder<Bond> >
{
private:
	BondTradeBookingService* bondService;
	// determine the trade book coming from bond execution service
	long count = 0; 
public:
	BondTradeServiceListener(BondTradeBookingService* TBS);
	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(ExecutionOrder<Bond>& data) override;
	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(ExecutionOrder<Bond>& data) override;
	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(ExecutionOrder<Bond>& data) override;
};

BondTradeServiceListener::BondTradeServiceListener(BondTradeBookingService* TBS) :
	bondService(TBS) {};

void BondTradeServiceListener::ProcessAdd(ExecutionOrder<Bond>& data)
{
	//const T & _product, 
	Bond product = data.GetProduct();
	//string _tradeId, 
	string tradeId = data.GetOrderId();
	//double _price, 
	double price = data.GetPrice();
	//long _quantity, 
	long quantity = data.GetHiddenQuantity() + data.GetVisibleQuantity();
	//Side _side
	Side side;
	if (data.GetPricingSide() == BID) side = SELL;
	else side = BUY;

	//string _book, 
	string book = "TRSY" + to_string((count++) % 3 + 1);
	Trade<Bond> trade(product, tradeId, price, book, quantity, side);
	bondService->OnMessage(trade);
}

void BondTradeServiceListener::ProcessRemove(ExecutionOrder<Bond>& data) {};
void BondTradeServiceListener::ProcessUpdate(ExecutionOrder<Bond>& data) {};

#endif
