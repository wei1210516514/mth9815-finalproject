#ifndef BONDALGOEXECUTIONSERVICE_HPP
#define BONDALGOEXECUTIONSERVICE_HPP

/*
	Bond algo execution service
	Object: AlgoExecution
	Derive:
	bondalgoexecution service -> service
	bondalgoexecution servicelistener -> servicelistener
*/

#include <iostream>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <map>
#include <string>
#include "soa.hpp"
#include "products.hpp"
#include "executionservice.hpp"
#include "inputdatageneration.h"
#include "bondmarketdataservice.h"

//AlgoExecution object
template<typename T>
class AlgoExecution
{
private:
	ExecutionOrder<T>* exe_order;
public:
	AlgoExecution() {};
	AlgoExecution(const T& product, PricingSide side, string orderId, OrderType orderType, double price, long visibleQuantity, long hiddenQuantity, string parentOrderId, bool isChildOrder);

	ExecutionOrder<T>* GetExecutionOrder() const;
};

template<typename T>
AlgoExecution<T>::AlgoExecution(const T& product, PricingSide side, string orderId, OrderType orderType, double price,
	long visibleQuantity, long hiddenQuantity, string parentOrderId, bool isChildOrder)
{
	exe_order = new ExecutionOrder<T>(product, side, orderId, orderType, price, visibleQuantity, hiddenQuantity, parentOrderId, isChildOrder);
}

template<typename T>
ExecutionOrder<T>* AlgoExecution<T>::GetExecutionOrder() const
{
	return exe_order;
}


class BondAlgoExecutionService : public Service< string, AlgoExecution<Bond> >
{
private:
	map< string, AlgoExecution<Bond> > AlgoService;
	vector< ServiceListener< AlgoExecution<Bond> >* > Listeners;
	int count;  // determine the side of the algo execution
public:
	BondAlgoExecutionService() :count(0) {};

	// Get data on our service given a key
	virtual AlgoExecution<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(AlgoExecution<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events for data to the Service
	virtual void AddListener(ServiceListener<AlgoExecution<Bond>>* listener) override;

	// Get all listeners on the Service
	virtual const vector< ServiceListener<AlgoExecution<Bond> >* >& GetListeners() const override;

	// Execute an order on a market
	void ExecuteOrder(OrderBook<Bond>& orderBook);
};

AlgoExecution<Bond>& BondAlgoExecutionService::GetData(string key)
{
	return AlgoService[key];
}

void BondAlgoExecutionService::OnMessage(AlgoExecution<Bond>& data)
{
	AlgoService[data.GetExecutionOrder()->GetProduct().GetProductId()] = data;
}

void BondAlgoExecutionService::AddListener(ServiceListener<AlgoExecution<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<AlgoExecution<Bond> >* >& BondAlgoExecutionService::GetListeners() const
{
	return Listeners;
}

void BondAlgoExecutionService::ExecuteOrder(OrderBook<Bond>& orderBook)
{
	Bond product = orderBook.GetProduct();
	string productId = product.GetProductId();
	BidOffer temp = orderBook.GetBestBidOffer();
	double bidPrice = temp.GetBidOrder().GetPrice(); 
	double offerPrice = temp.GetOfferOrder().GetPrice();

	//aggressing when the spread is at its tightest
	if (offerPrice - bidPrice <= 1.0/128)
	{
		//const T& product: product(define above) 
		
		//PricingSide side 
		PricingSide _side_;
		if (count % 2 == 0) _side_ = OFFER;
		else _side_ = BID;
		++count;

		//string orderId
		string orderId = generate_tradeId(1,rand())[0];

		//OrderType orderType, 
		OrderType ordertype = IOC;

		//bool isChildOrder
		bool ischild = false;

		//string parentOrderId
		string parentorderId = "";

		//double price, long visiblequantity, hiddenquantity
		double price; long visiblequantity, hiddenquantity;
		if (_side_ == OFFER)
		{
			price = offerPrice;
			long quantity = temp.GetOfferOrder().GetQuantity();
			visiblequantity = quantity / 3;  //1:2
			hiddenquantity = quantity - visiblequantity;
		}
		else 
		{
			price = bidPrice;
			long quantity = temp.GetBidOrder().GetQuantity();
			visiblequantity = quantity / 3;  //1:2
			hiddenquantity = quantity - visiblequantity;
		}
		AlgoExecution<Bond> result(product, _side_, orderId, ordertype, price, visiblequantity, hiddenquantity, parentorderId, ischild);
		for (int i = 0; i < Listeners.size(); ++i)
			Listeners[i]->ProcessAdd(result);
	}
}

class BondAlgoExecutionServiceListener :public ServiceListener< OrderBook<Bond> >
{
private:
	BondAlgoExecutionService* bondService;
public:
	BondAlgoExecutionServiceListener(BondAlgoExecutionService* BAES);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(OrderBook<Bond>& data) override;

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(OrderBook<Bond>& data) override;

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(OrderBook<Bond>& data) override;
};

BondAlgoExecutionServiceListener::BondAlgoExecutionServiceListener(BondAlgoExecutionService* BAES) :
	bondService(BAES) {}

void BondAlgoExecutionServiceListener::ProcessAdd(OrderBook<Bond>& data)
{
	bondService->ExecuteOrder(data);
}

void BondAlgoExecutionServiceListener::ProcessRemove(OrderBook<Bond>& data) {}
void BondAlgoExecutionServiceListener::ProcessUpdate(OrderBook<Bond>& data) {}

#endif
