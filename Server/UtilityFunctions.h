#ifndef UTILITYFUNCTIONS_HPP
#define UTILITYFUNCTIONS_HPP

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "products.hpp"
#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/asio.hpp>
using namespace boost::asio;
using ip::tcp;
using std::string;
using namespace std;
using namespace boost;

Bond getBond(string productId)
{
	Bond bond;
	if (productId == "912828M72") bond = Bond("912828M72", CUSIP, "US2Y", 0.01750, from_string("2021/11/23"));
	if (productId == "912828G20") bond = Bond("912828G20", CUSIP, "US3Y", 0.01875, from_string("2022/12/15"));
	if (productId == "912828UA6") bond = Bond("912828UA6", CUSIP, "US5Y", 0.02000, from_string("2024/11/30"));
	if (productId == "912828PK0") bond = Bond("912828PK0", CUSIP, "US7Y", 0.02125, from_string("2026/11/30"));
	if (productId == "912828HR4") bond = Bond("912828HR4", CUSIP, "US10Y", 0.02250, from_string("2029/12/15"));
	if (productId == "912810SJ8") bond = Bond("912810SJ8", CUSIP, "US30Y", 0.02750, from_string("2049/12/15"));
	return bond;
}

string getBucketedType(string productId)
{
	if (productId == "912828M72" || productId == "912828G20") return "FrontEnd";
	if (productId == "912828UA6" || productId == "912828PK0" || productId == "912828HR4") return "Belly";
	if (productId == "912810SJ8") return "LongEnd";
}

double String2Double(string str)
{
	double result = 0.0;
	if (str.size() == 6)
	{
		result = stod(str.substr(0, 2)) + stod(str.substr(3, 2))/32;
		if (str[5] == '+') result += 4.0/256;
		else result += 1.0*stod(str.substr(5, 1))/256;
	}
	if (str.size() == 7)
	{
		result = stod(str.substr(0, 3))+stod(str.substr(4, 2))/32;
		if (str[6] == '+') result += 4.0/256;
		else result += 1.0*stod(str.substr(6, 1))/256;
	}
	return result;
}

string Double2String(double price)
{
	int integer = floor(price);
	int decimal = floor((price - integer) * 256.0);
	int first_two = floor(decimal/8.0);
	int last = decimal % 8;

	string _integer = to_string(integer);
	string _first_two = to_string(first_two);
	string _last = to_string(last);
	if (first_two < 10) _first_two = "0" + _first_two;
	if (last == 4) _last = "+";
	
	return _integer + "-" + _first_two + _last;
}

string CurrentTime(bool millisec)
{
	/*
	if millisec = true -> only need millisecond
	if millisec = false -> cast from year to millisecond
	*/
	auto current = posix_time::microsec_clock::local_time();
	string temp = to_iso_extended_string(current),tmp = ""; //2019-12-22T14:56:54.648915
	vector<string> time_section;
	for (int i = 0; i < temp.size(); ++i)
	{
		if (temp[i] == 'T') tmp += " ";
		else if (temp[i] == '.')
		{
			time_section.push_back(tmp+".");
			tmp = "";
		}
		else
		{
			tmp += temp[i];
		}
	}
	time_section.push_back(tmp);
	if (millisec) return time_section[1].substr(0, 3);
	else
	{
		return time_section[0] + time_section[1].substr(0, 3);
	}
}

string read_(tcp::socket& socket) {
	boost::asio::streambuf buf;
	boost::asio::read_until(socket, buf, "\n");
	string data = boost::asio::buffer_cast<const char*>(buf.data());
	return data.substr(0, data.size() - 1);
}

void send_(tcp::socket& socket, const string& message) {
	const string msg = message + "\n";
	boost::asio::write(socket, boost::asio::buffer(message));
}


#endif
