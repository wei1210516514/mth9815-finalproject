#ifndef BONDRISKHISTORICALDATASERVICE_HPP
#define BONDRISKHISTORICALDATASERVICE_HPP

/*
	BondRiskService -> HistoricalDataService->risk.txt
*/

#include <vector>
#include <map>
#include "UtilityFunctions.h"
#include "outputdatageneration.h"
using namespace std;

/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */

class BondRiskHistoricalDataConnector;

class BondRiskHistoricalDataService : public Service<string, PV01<Bond> >
{
private:
	vector<ServiceListener<PV01<Bond> >*> Listeners;
	BondRiskHistoricalDataConnector* bondTConnector;
	map<string, PV01<Bond> > bondT;
	map<string, PV01<BucketedSector<Bond> > > bucketT;
public:
	BondRiskHistoricalDataService();

	// Get data on our service given a key
	virtual PV01<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(PV01<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PV01<Bond> >* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<PV01<Bond> >* >& GetListeners() const override;

	BondRiskHistoricalDataConnector* GetConnector() const;

	// Persist data to a store
	void PersistData(string persistKey, const PV01<Bond>& data);
	void PersistData(string persistKey, const PV01<BucketedSector<Bond> >& data);

	// Update the bucketed risk for the bucket sector
	virtual void UpdateBucketedRisk(const BucketedSector<Bond>& sector);

	// Get the bucketed risk for the bucket sector
	virtual const PV01<BucketedSector<Bond> > GetBucketedRisk(const string name) const;
};

class BondRiskHistoricalDataConnector : public Connector<PV01<Bond> >
{
private:
	BondRiskHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondRiskHistoricalDataConnector(BondRiskHistoricalDataService* HDS);

	// Publish data to the Connector
	void Publish(PV01<Bond>& data);
	void Publish(PV01<BucketedSector<Bond> >& data);
};


BondRiskHistoricalDataConnector::BondRiskHistoricalDataConnector(BondRiskHistoricalDataService* HDS) :
	bondService(HDS) {}

void BondRiskHistoricalDataConnector::Publish(PV01<Bond>& data)
{
	RiskBondOutput(data);
}

void BondRiskHistoricalDataConnector::Publish(PV01<BucketedSector<Bond> >& data)
{
	RiskBucketedOutput(data);
}

BondRiskHistoricalDataService::BondRiskHistoricalDataService()
{
	bondTConnector = new BondRiskHistoricalDataConnector(this);
}

PV01<Bond>& BondRiskHistoricalDataService::GetData(string key)
{
	return bondT[key];
}

void BondRiskHistoricalDataService::OnMessage(PV01<Bond>& data)
{}

void BondRiskHistoricalDataService::AddListener(ServiceListener<PV01<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<PV01<Bond> >* >& BondRiskHistoricalDataService::GetListeners() const
{
	return Listeners;
}

BondRiskHistoricalDataConnector* BondRiskHistoricalDataService::GetConnector() const
{
	return bondTConnector;
}

void BondRiskHistoricalDataService::PersistData(string persistKey, const PV01<Bond>& data)
{
	bondT[persistKey] = data;
	PV01<Bond> temp = data;
	bondTConnector->Publish(temp);
}

void BondRiskHistoricalDataService::PersistData(string persistKey, const PV01<BucketedSector<Bond> >& data)
{
	bucketT[persistKey] = data;
	PV01<BucketedSector<Bond> > temp(data);
	bondTConnector->Publish(temp);
}

void BondRiskHistoricalDataService::UpdateBucketedRisk(const BucketedSector<Bond>& sector)
{
	std::vector<Bond> products = sector.GetProducts();
	string duration = sector.GetName();
	string productId;
	long sum_quantity = 0;
	double sum_pv01 = 0, unit_pv01 = 0.0;

	// calculate the overall pv01 and the overall quantity
	for (int i = 0; i < products.size(); ++i)
	{
		productId = products[i].GetProductId();
		if (getBucketedType(productId) == duration)
		{
			sum_quantity += bondT[productId].GetQuantity();
			sum_pv01 += bondT.at(productId).GetPV01() * bondT.at(productId).GetQuantity();
		}
	}

	if (sum_quantity > 0) unit_pv01 = sum_pv01 / sum_quantity;

	PV01<BucketedSector<Bond>> newbucket(sector, unit_pv01, sum_quantity);
	bucketT[sector.GetName()] = newbucket;
}

const PV01<BucketedSector<Bond>> BondRiskHistoricalDataService::GetBucketedRisk(const string name) const
{
	if (bucketT.find(name) != bucketT.end())
	{
		return bucketT.at(name);
	}
	else  //create a new one
	{
		Bond _product_ = getBond(name);
		vector<Bond> temp{ _product_ };
		BucketedSector<Bond> bs(temp, name);
		PV01<BucketedSector<Bond> >  result(bs, 0, 0);
		return result;
	}
}

//listener from higher-level operations
class BondRiskHistoricalDataServiceListener : public ServiceListener<PV01<Bond> >
{
private:
	BondRiskHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondRiskHistoricalDataServiceListener(BondRiskHistoricalDataService* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(PV01<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(PV01<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(PV01<Bond>& data);
};

BondRiskHistoricalDataServiceListener::BondRiskHistoricalDataServiceListener(BondRiskHistoricalDataService* HDSL) :
	bondService(HDSL) {}

void BondRiskHistoricalDataServiceListener::ProcessAdd(PV01<Bond>& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);

	//update the bucketed sector 
	string productId = data.GetProduct().GetProductId();
	string bucketsector = getBucketedType(productId);
	Bond product = data.GetProduct();
	PV01<BucketedSector<Bond>> old = bondService->GetBucketedRisk(bucketsector);
	vector<Bond> temp = old.GetProduct().GetProducts();
	long quantity = old.GetQuantity();
	if (temp.size() == 1 && quantity == 0)  //the first one
	{
		temp.pop_back();
		temp.push_back(product);
		BucketedSector<Bond> bs(temp, bucketsector);
		bondService->UpdateBucketedRisk(bs);
	}
	else  //if already have
	{
		temp.push_back(product);
		BucketedSector<Bond> bs(temp, bucketsector);
		bondService->UpdateBucketedRisk(bs);
	}

	PV01<BucketedSector<Bond>> newbucket = bondService->GetBucketedRisk(bucketsector);
	bondService->PersistData(_persistKey_, newbucket);
}

void BondRiskHistoricalDataServiceListener::ProcessRemove(PV01<Bond>& data) {}
void BondRiskHistoricalDataServiceListener::ProcessUpdate(PV01<Bond>& data) {}

#endif
