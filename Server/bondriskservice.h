#ifndef BONDRISKSERVICE_HPP
#define BONDRISKSERVICE_HPP

/*
	Bond risk service
	Derive:
	bondriskservice -> service
	bondriskservicelistener -> servicelistener
*/

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "soa.hpp"
#include "products.hpp"
#include "UtilityFunctions.h"
#include "positionservice.hpp"
#include "riskservice.hpp"
using namespace std;

/**
 * Bond Risk Service to vend out risk for a particular security and across a risk bucketed sector.
 * Keyed on product identifier.
 * Type T is the product type.
 */
class BondRiskService :public Service< string, PV01<Bond> >
{
private:
	map< string, PV01<Bond> > PVRisk;
	vector< ServiceListener< PV01<Bond> >* > Listeners;

	// Get PV01 value for US Treasury 2Y, 3Y, 5Y, 7Y, 10Y, and 30Y.
	double GetPV01(string productId)
	{
		double pv01 = 0;
		if (productId == "912828M72") pv01 = 0.01948992;
		if (productId == "912828G20") pv01 = 0.02865304;
		if (productId == "912828UA6") pv01 = 0.04581119;
		if (productId == "912828PK0") pv01 = 0.06127718;
		if (productId == "912828HR4") pv01 = 0.08161449;
		if (productId == "912810SJ8") pv01 = 0.15013155;
		return pv01;
	}
public:
	BondRiskService() {};

	//no subsequent listeners
	virtual void OnMessage(PV01<Bond>& data) override;
	
	// Get data on our service given a key
	virtual PV01<Bond>& GetData(string key) override;
	
	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PV01<Bond> >* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<PV01<Bond> >* >& GetListeners() const override;

	//add a position that the service will risk
	void AddPosition(Position<Bond>& position);
};

PV01<Bond>& BondRiskService::GetData(string key)
{
	return PVRisk[key];
}

void BondRiskService::OnMessage(PV01<Bond>& data) 
{
	PVRisk[data.GetProduct().GetProductId()] = data;
}

void BondRiskService::AddListener(ServiceListener<PV01<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<PV01<Bond> >* >& BondRiskService::GetListeners() const
{
	return Listeners;
}

void BondRiskService::AddPosition(Position<Bond>& position)
{
	Bond _product_ = position.GetProduct();
	string _productId_ = _product_.GetProductId();
	double _PV01_ = GetPV01(_productId_);
	long quantity = position.GetAggregatePosition();
	PV01<Bond> temp(_product_, _PV01_, quantity);
	OnMessage(temp);

    //transfer the information 
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(temp);
}

//listener (from position)
class BondRiskServiceListener :public ServiceListener<Position<Bond>>
{
private:
	BondRiskService* bondService;
public:
	BondRiskServiceListener(BondRiskService* BRS);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Position<Bond>& data);

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Position<Bond>& data);

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Position<Bond>& data);
};

BondRiskServiceListener::BondRiskServiceListener(BondRiskService* BRS) :bondService(BRS) {}

void BondRiskServiceListener::ProcessAdd(Position<Bond>& data)
{
	bondService->AddPosition(data);
}

void BondRiskServiceListener::ProcessRemove(Position<Bond>& data) {}

void BondRiskServiceListener::ProcessUpdate(Position<Bond>& data) {}

#endif
