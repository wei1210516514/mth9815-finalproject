/**
 * marketdataservice.hpp
 * Defines the data types and Service for order book market data.
 *
 * @author Breman Thuraisingham
 */
#ifndef MARKET_DATA_SERVICE_HPP
#define MARKET_DATA_SERVICE_HPP

#include <string>
#include <vector>
#include "soa.hpp"

using namespace std;

// Side for market data
enum PricingSide { BID, OFFER };

/**
 * A market data order with price, quantity, and side.
 */
class Order
{

public:

  // ctor for an order
	Order() {};
	Order(double _price, long _quantity, PricingSide _side);
	Order(const Order& ord);
	Order& operator=(const Order& ord);

	// Get the price on the order
	double GetPrice() const;

	// Get the quantity on the order
	long GetQuantity() const;

	// Get the side on the order
	PricingSide GetSide() const;

private:
  double price;
  long quantity;
  PricingSide side;

};

/**
 * Class representing a bid and offer order
 */
class BidOffer
{

public:

	// ctor for bid/offer
	BidOffer(const Order &_bidOrder, const Order &_offerOrder);
	BidOffer(const BidOffer& bo);
	BidOffer& operator=(const BidOffer& bo);

	// Get the bid order
	const Order& GetBidOrder() const;

	// Get the offer order
	const Order& GetOfferOrder() const;

private:
  Order bidOrder;
  Order offerOrder;

};

/**
 * Order book with a bid and offer stack.
 * Type T is the product type.
 */
template<typename T>
class OrderBook
{

public:

  // ctor for the order book
	OrderBook() {};
	OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack);

	// Get the product
	const T& GetProduct() const;

	// Get the bid stack
	const vector<Order>& GetBidStack() const;

	// Get the offer stack
	const vector<Order>& GetOfferStack() const;

	const BidOffer GetBestBidOffer() const;

private:
  T product;
  vector<Order> bidStack;
  vector<Order> offerStack;

};

Order::Order(double _price, long _quantity, PricingSide _side)
{
  price = _price;
  quantity = _quantity;
  side = _side;
}

Order::Order(const Order& ord)
{
	price = ord.GetPrice();
	quantity = ord.GetQuantity();
	side = ord.GetSide();
}

Order& Order::operator=(const Order& ord)
{
	price = ord.GetPrice();
	quantity = ord.GetQuantity();
	side = ord.GetSide();
	return *this;
}

double Order::GetPrice() const
{
  return price;
}
 
long Order::GetQuantity() const
{
  return quantity;
}
 
PricingSide Order::GetSide() const
{
  return side;
}

BidOffer::BidOffer(const Order &_bidOrder, const Order &_offerOrder) :
  bidOrder(_bidOrder), offerOrder(_offerOrder)
{
}

BidOffer::BidOffer(const BidOffer& bo)
{
	bidOrder = bo.GetBidOrder();
	offerOrder = bo.GetOfferOrder();
}

BidOffer& BidOffer::operator=(const BidOffer& bo)
{
	bidOrder = bo.GetBidOrder();
	offerOrder = bo.GetOfferOrder();
	return *this;
}



const Order& BidOffer::GetBidOrder() const
{
  return bidOrder;
}

const Order& BidOffer::GetOfferOrder() const
{
  return offerOrder;
}

template<typename T>
OrderBook<T>::OrderBook(const T &_product, const vector<Order> &_bidStack, const vector<Order> &_offerStack) :
  product(_product), bidStack(_bidStack), offerStack(_offerStack)
{
}

template<typename T>
const T& OrderBook<T>::GetProduct() const
{
  return product;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetBidStack() const
{
  return bidStack;
}

template<typename T>
const vector<Order>& OrderBook<T>::GetOfferStack() const
{
  return offerStack;
}

template<typename T>
const BidOffer OrderBook<T>::GetBestBidOffer() const
{
	vector<Order> bidpart = bidStack, askpart = offerStack;
	double bid_price = 0.0, ask_price = 100000000000.0;
	Order bid_order, ask_order;
	for (int i = 0; i < bidpart.size(); ++i)
	{
		if (bidpart[i].GetPrice() > bid_price)
		{
			bid_order = bidpart[i];
			bid_price = bidpart[i].GetPrice();
		}
	}
	for (int i = 0; i < askpart.size(); ++i)
	{
		if (askpart[i].GetPrice() < ask_price)
		{
			ask_order = askpart[i];
			ask_price = askpart[i].GetPrice();
		}
	}
	BidOffer result(bid_order, ask_order);
	return result;
}

#endif
