#ifndef BONDEXECUTIONHISTORICALDATASERVICE_HPP
#define BONDEXECUTIONHISTORICALDATASERVICE_HPP
/*
	BondExecutionService -> HistoricalDataService->executions.txt
*/

#include <vector>
#include <map>
#include "UtilityFunctions.h"
#include "outputdatageneration.h"
using namespace std;

class BondExecutionHistoricalDataConnector;


class BondExecutionHistoricalDataService : public Service<string,ExecutionOrder<Bond> >
{
private:
	vector<ServiceListener<ExecutionOrder<Bond> >* > Listeners;
	BondExecutionHistoricalDataConnector* bondTConnector;
	map<string, ExecutionOrder<Bond> > bondT;
public:
	BondExecutionHistoricalDataService();

	// Get data on our service given a key
	virtual ExecutionOrder<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(ExecutionOrder<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<ExecutionOrder<Bond> >* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<ExecutionOrder<Bond> >* >& GetListeners() const override;

	BondExecutionHistoricalDataConnector* GetConnector() const;

	// Persist data to a store
	void PersistData(string persistKey, const ExecutionOrder<Bond>& data);
};


class BondExecutionHistoricalDataConnector : public Connector<ExecutionOrder<Bond> >
{
private:
	BondExecutionHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondExecutionHistoricalDataConnector(BondExecutionHistoricalDataService* HDS);

	// Publish data to the Connector
	void Publish(ExecutionOrder<Bond>& data);
};


BondExecutionHistoricalDataConnector::BondExecutionHistoricalDataConnector(BondExecutionHistoricalDataService* HDS) :
	bondService(HDS) {}

void BondExecutionHistoricalDataConnector::Publish(ExecutionOrder<Bond>& data)
{
	ExecutionOutput(data);
}


BondExecutionHistoricalDataService::BondExecutionHistoricalDataService()
{
	bondTConnector = new BondExecutionHistoricalDataConnector(this);
}

ExecutionOrder<Bond>& BondExecutionHistoricalDataService::GetData(string key)
{
	return bondT[key];
}

void BondExecutionHistoricalDataService::OnMessage(ExecutionOrder<Bond>& data) {}

void BondExecutionHistoricalDataService::AddListener(ServiceListener<ExecutionOrder<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<ExecutionOrder<Bond> >* >& BondExecutionHistoricalDataService::GetListeners() const
{
	return Listeners;
}

BondExecutionHistoricalDataConnector* BondExecutionHistoricalDataService::GetConnector() const
{
	return bondTConnector;
}

void BondExecutionHistoricalDataService::PersistData(string persistKey, const ExecutionOrder<Bond>& data)
{
	bondT[persistKey] = data;
	ExecutionOrder<Bond> temp = data;
	bondTConnector->Publish(temp);
}

//listener from higher-level operations
class BondExecutionHistoricalDataServiceListener : public ServiceListener<ExecutionOrder<Bond> >
{
private:
	BondExecutionHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondExecutionHistoricalDataServiceListener(BondExecutionHistoricalDataService* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(ExecutionOrder<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(ExecutionOrder<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(ExecutionOrder<Bond>& data);
};

BondExecutionHistoricalDataServiceListener::BondExecutionHistoricalDataServiceListener(BondExecutionHistoricalDataService* HDSL) :
	bondService(HDSL) {}

void BondExecutionHistoricalDataServiceListener::ProcessAdd(ExecutionOrder<Bond>& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);
}

void BondExecutionHistoricalDataServiceListener::ProcessRemove(ExecutionOrder<Bond>& data) {}
void BondExecutionHistoricalDataServiceListener::ProcessUpdate(ExecutionOrder<Bond>& data) {}

#endif
