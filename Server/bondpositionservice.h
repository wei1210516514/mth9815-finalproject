#ifndef BONDPOSITIONSERVICE_HPP
#define BONDPOSITIONSERVICE_HPP

/*
	Bond position service
	Derive:
	bondpositionservice -> service
	bondpositionservicelistener -> servicelistener
*/

#include <iostream>
#include <string>
#include <map>
#include "positionservice.hpp"
#include "soa.hpp"
#include "UtilityFunctions.h"
using namespace std;

class BondPositionService :public Service< string, Position<Bond> >
{
private:
	map< string, Position<Bond> > positionsService;
	vector< ServiceListener< Position<Bond> >* > Listeners;
public:
	BondPositionService() {};
	virtual void OnMessage(Position<Bond>& data) override;
	// Get data on our service given a key
	virtual Position<Bond>& GetData(string key) override;
	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Position<Bond> >* listener);

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Position<Bond> >* >& GetListeners() const;
	
	// Add a trade to the bondriskservice
	virtual void AddTrade(const Trade<Bond>& trade);
};

void BondPositionService::OnMessage(Position<Bond>& data)
{
	positionsService.insert(pair< string, Position<Bond> >(data.GetProduct().GetProductId(), data));
}

Position<Bond>& BondPositionService::GetData(string key)
{
	return positionsService[key];
}

void BondPositionService::AddListener(ServiceListener<Position<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<Position<Bond> >* >& BondPositionService::GetListeners() const
{
	return Listeners;
}

void BondPositionService::AddTrade(const Trade<Bond>& trade)
{
	//add new trade and update the trade
	string _productId_ = trade.GetProduct().GetProductId();
	string _book_ = trade.GetBook();
	Side _side_ = trade.GetSide();
	long quantity;
	if (_side_ == BUY) quantity = trade.GetQuantity();
	else quantity = -trade.GetQuantity();

	//update the position in the positionservice.hpp
	if (positionsService.find(_productId_) != positionsService.end())
	{
		Position<Bond> pos = positionsService[_productId_];
		pos.position_summation(_book_, quantity);
		OnMessage(pos);  //here use to store not messaging
		//send to all listeners
		for (int i = 0; i < Listeners.size(); ++i)
			Listeners[i]->ProcessAdd(pos);
	}
	else
	{
		Position<Bond> pos(trade.GetProduct());
		pos.position_summation(_book_, quantity);
		OnMessage(pos);
		//send to all listeners
		for (int i = 0; i < Listeners.size(); ++i)
			Listeners[i]->ProcessAdd(pos);
	}
}

//listener from tradebookingservice
class BondPositionServiceListener :public ServiceListener< Trade<Bond> >
{
private:
	BondPositionService* bondService;
public:
	BondPositionServiceListener(BondPositionService* BPS);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Trade<Bond>& data);

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Trade<Bond>& data);

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Trade<Bond>& data);
};

BondPositionServiceListener::BondPositionServiceListener(BondPositionService* BPS) :bondService(BPS) {};

void BondPositionServiceListener::ProcessAdd(Trade<Bond>& data)
{
	bondService->AddTrade(data);
}

void BondPositionServiceListener::ProcessRemove(Trade<Bond>& data) {};

// Listener callback to process an update event to the Service
void BondPositionServiceListener::ProcessUpdate(Trade<Bond>& data) {};

#endif
