#ifndef BONDINQUIRYHISTORICALDATASERVICE_HPP
#define BONDINQUIRYHISTORICALDATASERVICE_HPP

/*
	BondInquiryService -> HistoricalDataService->inquiry.txt
*/

#include <vector>
#include <map>
#include "UtilityFunctions.h"
#include "outputdatageneration.h"
using namespace std;


class BondInquiryHistoricalDataConnector;


class BondInquiryHistoricalDataService : public Service<string, Inquiry<Bond> >
{
private:
	vector<ServiceListener<Inquiry<Bond> >*> Listeners;
	BondInquiryHistoricalDataConnector* bondTConnector;
	map<string, Inquiry<Bond> > bondT;
public:
	BondInquiryHistoricalDataService();

	// Get data on our service given a key
	virtual Inquiry<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Inquiry<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Inquiry<Bond> >* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<Inquiry<Bond> >* >& GetListeners() const override;

	BondInquiryHistoricalDataConnector* GetConnector() const;

	// Persist data to a store
	void PersistData(string persistKey, const Inquiry<Bond>& data);
};

class BondInquiryHistoricalDataConnector : public Connector<Inquiry<Bond> >
{
private:
	BondInquiryHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondInquiryHistoricalDataConnector(BondInquiryHistoricalDataService* HDS);

	// Publish data to the Connector
	void Publish(Inquiry<Bond>& data);
};


BondInquiryHistoricalDataConnector::BondInquiryHistoricalDataConnector(BondInquiryHistoricalDataService* HDS) :
	bondService(HDS){}

void BondInquiryHistoricalDataConnector::Publish(Inquiry<Bond>& data)
{
	InquiryOutput(data);
}

BondInquiryHistoricalDataService::BondInquiryHistoricalDataService()
{
	bondTConnector = new BondInquiryHistoricalDataConnector(this);
}


Inquiry<Bond>& BondInquiryHistoricalDataService::GetData(string key)
{
	return bondT[key];
}

void BondInquiryHistoricalDataService::OnMessage(Inquiry<Bond>& data) {}

void BondInquiryHistoricalDataService::AddListener(ServiceListener<Inquiry<Bond> >* listener)
{
	Listeners.push_back(listener);
}


const vector< ServiceListener<Inquiry<Bond> >* >& BondInquiryHistoricalDataService::GetListeners() const
{
	return Listeners;
}

BondInquiryHistoricalDataConnector* BondInquiryHistoricalDataService::GetConnector() const
{
	return bondTConnector;
}

void BondInquiryHistoricalDataService::PersistData(string persistKey, const Inquiry<Bond>& data)
{
	bondT[persistKey] = data;
	Inquiry<Bond> temp = data;
	bondTConnector->Publish(temp);
}

//listener from higher-level operations
class BondInquiryHistoricalDataServiceListener : public ServiceListener<Inquiry<Bond> >
{
private:
	BondInquiryHistoricalDataService* bondService;
public:
	// Connector and Destructor
	BondInquiryHistoricalDataServiceListener(BondInquiryHistoricalDataService* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(Inquiry<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(Inquiry<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(Inquiry<Bond>& data);
};


BondInquiryHistoricalDataServiceListener::BondInquiryHistoricalDataServiceListener(BondInquiryHistoricalDataService* HDSL) :
	bondService(HDSL) {}

void BondInquiryHistoricalDataServiceListener::ProcessAdd(Inquiry<Bond>& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);
}

void BondInquiryHistoricalDataServiceListener::ProcessRemove(Inquiry<Bond>& data) {}
void BondInquiryHistoricalDataServiceListener::ProcessUpdate(Inquiry<Bond>& data) {}

#endif
