#ifndef BONDEXECUTIONSERVICE_HPP
#define BONDEXECUTIONSERVICE_HPP

/*
	Bond execution service
	Derive:
	bondexecution service -> service
	bondexecution servicelistener -> servicelistener
*/

#include <iostream>
#include <map>
#include <string>
#include "bondalgoexecutionservice.h"

/**
 * Service for executing orders on an exchange.
 * Keyed on product identifier.
 * Type T is the product type.
 */
class BondExecutionService :public Service< string, ExecutionOrder<Bond> >
{
private:
	map< string, ExecutionOrder<Bond> > ExecutionService;
	vector< ServiceListener< ExecutionOrder<Bond> >* > Listeners;
public:
	// Constructor and destructor
	BondExecutionService() {};

	// Get data on our service given a key
	virtual ExecutionOrder<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(ExecutionOrder<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events for data to the Service
	virtual void AddListener(ServiceListener<ExecutionOrder<Bond>>* listener) override;

	// Get all listeners on the Service
	virtual const vector<ServiceListener<ExecutionOrder<Bond>>*>& GetListeners() const override;

	// Execute an order on a market
	void ExecuteOrder(const ExecutionOrder<Bond>& order);
};

ExecutionOrder<Bond>& BondExecutionService::GetData(string key)
{
	return ExecutionService[key];
}

void BondExecutionService::OnMessage(ExecutionOrder<Bond>& data)
{
	ExecuteOrder(data);
}

void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector<ServiceListener<ExecutionOrder<Bond>>*>& BondExecutionService::GetListeners() const
{
	return Listeners;
}

void BondExecutionService::ExecuteOrder(const ExecutionOrder<Bond>& order)
{
	ExecutionService[order.GetProduct().GetProductId()] = order;
	ExecutionOrder<Bond> temp = order;
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(temp);
}

//listener (from algoexecution)
class BondExecutionServiceListener :public ServiceListener<AlgoExecution<Bond> >
{
private:
	BondExecutionService* bondService;
public:
	BondExecutionServiceListener(BondExecutionService* BESL);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(AlgoExecution<Bond>& data) override;

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(AlgoExecution<Bond>& data) override;

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(AlgoExecution<Bond>& data) override;
};

BondExecutionServiceListener::BondExecutionServiceListener(BondExecutionService* BESL) :
	bondService(BESL) {}

void BondExecutionServiceListener::ProcessAdd(AlgoExecution<Bond>& data)
{
	bondService->OnMessage(*data.GetExecutionOrder());
}

void BondExecutionServiceListener::ProcessRemove(AlgoExecution<Bond>& data) {}
void BondExecutionServiceListener::ProcessUpdate(AlgoExecution<Bond>& data) {}

#endif
