#ifndef BONDSTREAMINGSERVICE_HPP
#define BONDSTREAMINGSERVICE_HPP

/*
	Bond streaming service
	Derive:
	bondstreaming service -> service
	bondstreaming servicelistener -> servicelistener
*/

#include <iostream>
#include <unordered_map>
#include <map>
#include "products.hpp"
#include "bondalgostreamingservice.h"

class BondStreamingService :public Service<string, PriceStream <Bond> >
{
private:
	map< string, PriceStream<Bond> > bondstreamingservice;
	vector< ServiceListener< PriceStream<Bond> >* > Listeners;
public:
	BondStreamingService() {} // empty ctor

	// Get data on our service given a key
	virtual PriceStream<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(PriceStream<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<PriceStream<Bond>>* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<PriceStream<Bond>>* >& GetListeners() const override;

	// Publish two-way prices
	void PublishPrice(const PriceStream<Bond>& pricestream);
};

PriceStream<Bond>& BondStreamingService::GetData(string key)
{
	return bondstreamingservice[key];
}

void BondStreamingService::OnMessage(PriceStream<Bond>& data)
{
	bondstreamingservice[data.GetProduct().GetProductId()] = data;
}

void BondStreamingService::AddListener(ServiceListener<PriceStream<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<PriceStream<Bond>>* >& BondStreamingService::GetListeners() const
{
	return Listeners;
}

void BondStreamingService::PublishPrice(const PriceStream<Bond>& pricestream)
{
	bondstreamingservice[pricestream.GetProduct().GetProductId()] = pricestream;
	PriceStream<Bond> temp(pricestream);
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(temp);
}

//listener (from algo stream)
class BondStreamingServiceListener : public ServiceListener<AlgoStream<Bond> >
{
private:
	BondStreamingService* bondService;
public:
	BondStreamingServiceListener(BondStreamingService* BSS); // ctor

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(AlgoStream<Bond>& data) override;

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(AlgoStream<Bond>& data) override;

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(AlgoStream<Bond>& data) override;
};

BondStreamingServiceListener::BondStreamingServiceListener(BondStreamingService* BSS) :
	bondService(BSS) {}

void BondStreamingServiceListener::ProcessAdd(AlgoStream<Bond>& data)
{
	bondService->PublishPrice(data.GetStream());
}

void BondStreamingServiceListener::ProcessRemove(AlgoStream<Bond>& data) {}
void BondStreamingServiceListener::ProcessUpdate(AlgoStream<Bond>& data) {}


#endif
