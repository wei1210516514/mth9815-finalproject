#ifndef INPUTDATAGENERATION_HPP
#define INPUTDATAGENERATION_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <random>
#include <vector>
#include <tuple>
#include "tradebookingservice.hpp"
using namespace std;

// CUSIPS root: 912828
vector<string> product_type = { "912828M72", // 2Y
								"912828G20", // 3Y
								"912828UA6", // 5Y
								"912828PK0", // 7Y
								"912828HR4", // 10Y
								"912810SJ8" };// 30Y

vector<string> generate_tradeId(int total_number,int seed)
{
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution(0, 35);
	vector<string> result;
	for (int i = 0; i < total_number; ++i)
	{
		string tmp = "7";
		for (int j = 0; j < 11; ++j)
		{
			int temp = distribution(generator);
			if (temp <= 9)	tmp += to_string(temp);
			else tmp += char(temp + 55);
		}
		result.push_back(tmp);
	}
	return result;
};

vector<string> generate_trade_price(int seed,int total_number)
{
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution1(0, 31);
	std::uniform_int_distribution<int> distribution2(0, 7);
	vector<string> result; string temp;int temp2;
	for (int i = 0; i < total_number; ++i)
	{
		temp = to_string(distribution2(generator));
		temp2 = distribution1(generator);
		if (temp == "4") temp = "+";
		if (temp2 < 10) result.push_back("99-0" + to_string(temp2) + temp);
		else result.push_back("99-" + to_string(temp2) + temp);
	}

	return result;
};

/*
	trade.txt file generator
*/ 
void trade_data_generate()
{
	cout << "Generating the trade data" << endl;
	ofstream tradefile;
	tradefile.open("trades.txt");

	// tradeId
	int seed_Id = 11;
	vector<string> tradeId = generate_tradeId(60,seed_Id);

	// 6 product type
	for (int i = 0; i < 6; ++i)
	{
		//product number
		string product_T = product_type[i]; int trade;

		//10 trades for each product
		for (int j = 0; j < 10; ++j)
		{
			// Side
			string SideNum, bidask;
			if (j % 2 == 0) {
				SideNum = "BUY"; bidask = "99-000";
			}
			else {
				SideNum = "SELL"; bidask = "100-000";
			}
			// trades
			if (j < 5) trade = 1000000 * (j + 1);
			else trade = 1000000 * (10 - j);
			// Result
			tradefile << product_T << "," << tradeId[i * 10 + j] << "," << bidask << ",TRSY" << (j + i) % 3 + 1 << "," << trade << "," << SideNum << endl;
		}
	}
	cout << "Finishing generating the trade data" << endl;
}

vector<string> generate_price_price(int seed, int total_number)
{
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution(1,2);
	vector<string> result; string temp_bid, temp_ask; 
	int temp1 = 0, temp2 = 99, temp3 = 0, diff;
	bool increase = true;
	for (int i = 0; i < total_number; ++i)
	{
		int temp11 = temp1, temp12 = temp2, temp13 = temp3;
		if (temp1 == 0 && temp2 == 99 && temp3 == 0) increase = true;
		//the bid price
		if (temp3 < 10)
		{
			if (temp1 == 4)	temp_bid = to_string(temp2) + "-0" + to_string(temp3) + "+";
			else temp_bid = to_string(temp2) + "-0" + to_string(temp3) + to_string(temp1);
		}
		else
		{
			if (temp1 == 4)	temp_bid = to_string(temp2) + "-" + to_string(temp3) + "+";
			else temp_bid = to_string(temp2) + "-" + to_string(temp3) + to_string(temp1);
		}
		//random number generator 1/128 or 1/64
		diff = distribution(generator)*2; temp11 += diff;
		//the ask price
		if (temp11 >= 8)
		{
			temp11 -= 8; temp13 += + 1;
		}
		if (temp13 >= 32)
		{
			temp13 -= 32; temp12 += 1;
		}
		if (temp12 == 101)
		{
			temp_ask = "101-000";
			increase = false;
		}
		else
		{
			if (temp13 < 10)
			{
				if (temp11 == 4)	temp_ask = to_string(temp12) + "-0" + to_string(temp13) + "+";
				else temp_ask = to_string(temp12) + "-0" + to_string(temp13) + to_string(temp11);
			}
			else
			{
				if (temp11 == 4)	temp_ask = to_string(temp12) + "-" + to_string(temp13) + "+";
				else temp_ask = to_string(temp12) + "-" + to_string(temp13) + to_string(temp11);
			}
		}
		if (increase)  //increase one
		{
			temp1 += 1;
			if (temp1 >= 8)
			{
				temp1 -= 8; temp3 += 1;
			}
			if (temp3 >= 32)
			{
				temp3 -= 32; temp2 += 1;
			}
		}
		else  //decrease one
		{
			temp1 -= 1;
			if (temp1 <= -1)
			{
				temp1 += 8; temp3 -= 1;
			}
			if (temp3 <= -1)
			{
				temp3 += 32; temp2 -= 1;
			}
		}
		result.push_back(temp_bid + "," + temp_ask);
	}
	

	return result;
};

/*
	price.txt file generator
*/
void price_data_generate()
{
	cout << "Generating the price data" << endl;
	ofstream pricefile;
	pricefile.open("prices.txt");

	vector<int> seed_bidask = { 7,11,13,17,19,23 };

	// 6 product type
	for (int i = 0; i < 6; ++i)
	{
		//product number
		string product_T = product_type[i];
		vector<string> bidask = generate_price_price(seed_bidask[i], 10000);
		//1000000 trades for each product
		for (int j = 0; j < 10000; ++j)
		{
			pricefile << product_T << "," << bidask[j] << endl;
		}
	}
	cout << "Finishing generating the price data" << endl;
}

string price_increase_decrease(string mid_price, int half_spread, bool updown)
{
	vector<int> temp;
	//split the string
	if (mid_price[0] == '9')
	{
		temp.push_back(stoi(mid_price.substr(0, 2)));
		temp.push_back(stoi(mid_price.substr(3, 2)));
		if (mid_price[5] == '+') temp.push_back(4);
		else temp.push_back(stoi(mid_price.substr(5, 1)));
	}

	if (mid_price[0] == '1')
	{
		temp.push_back(stoi(mid_price.substr(0, 3)));
		temp.push_back(stoi(mid_price.substr(4, 2)));
		if (mid_price[6] == '+') temp.push_back(4);
		else temp.push_back(stoi(mid_price.substr(6, 1)));
	}
	
	string result;
	if (updown) //true: up
	{
		temp[2] += half_spread;
		if (temp[2] >= 8) 
		{
			temp[2] -= 8; temp[1] += 1;
		}
		if (temp[1] >= 32)
		{
			temp[1] -= 32; temp[0] += 1;
		}
	}
	else  //false: down
	{
		temp[2] -= half_spread;
		if (temp[2] <= -1)
		{
			temp[2] += 8; temp[1] -= 1;
		}
		if (temp[1] <= -1)
		{
			temp[1] += 32; temp[0] -= 1;
		}
	}
	if (temp[1] >= 10)
	{
		if (temp[2] == 4) result = to_string(temp[0]) + "-" + to_string(temp[1]) + "+";
		else result = to_string(temp[0]) + "-" + to_string(temp[1]) + to_string(temp[2]);
	}
	else
	{
		if (temp[2] == 4) result = to_string(temp[0]) + "-0" + to_string(temp[1]) + "+";
		else result = to_string(temp[0]) + "-0" + to_string(temp[1]) + to_string(temp[2]);
	}
	return result;
}

vector< tuple<string,string> > generate_marketdata_price(int total_number)
{
	vector<double> spread = { 2,4,6,8,8,6,4,2 };
	vector< tuple<string,string> > result; string temp_bid, temp_ask;
	string mid_price = "99-000";
	bool increase = true;
	for (int i = 0; i < total_number; ++i)
	{
		temp_bid = price_increase_decrease(mid_price, spread[i%spread.size()] / 2, false);
		temp_ask = price_increase_decrease(mid_price, spread[i%spread.size()] / 2, true);
		mid_price = price_increase_decrease(mid_price, 1, increase);
		tuple<string, string> temp = make_tuple(temp_bid, temp_ask);
		result.push_back(temp);
		if (mid_price == "101-000") increase = false;
		if (mid_price == "99-000") increase = true;
	}
	return result;
};


/*
	marketdata.txt file generator
*/
void marketdata_generate()
{
	cout << "Generating the marketdata data" << endl;
	ofstream pricefile;
	pricefile.open("marketdata.txt");

	// 6 product type
	for (int i = 0; i < 6; ++i)
	{
		//product number
		string product_T = product_type[i]; int quantity;
		vector< tuple<string, string> > bidask = generate_marketdata_price(5000000);
		//1000000 trades for each product
		for (int j = 0; j < 5000000; ++j)
		{
			//bid or offer
			quantity = 10000000 * (j%5+1);
			pricefile << product_T << "," << get<0>(bidask[j]) << ",BID," << get<1>(bidask[j]) << ",OFFER," << quantity << endl;
		}
	}
	cout << "Finishing generating the marketdata data" << endl;
}

vector<string> generate_inquiry_price(int seed, int total_number)
{
	std::default_random_engine generator(seed);
	std::uniform_int_distribution<int> distribution1(0, 31);
	std::uniform_int_distribution<int> distribution2(0, 7);
	std::uniform_int_distribution<int> distribution3(99, 100);
	vector<string> result; string temp; int temp2, temp3;
	for (int i = 0; i < total_number; ++i)
	{
		temp = to_string(distribution2(generator));
		temp2 = distribution1(generator);
		temp3 = distribution3(generator);
		if (temp == "4") temp = "+";
		if (temp2 < 10) result.push_back(to_string(temp3) + "-0" + to_string(temp2) + temp);
		else result.push_back(to_string(temp3) + "-" + to_string(temp2) + temp);
	}
	return result;
};

/*
	inquiry.txt file generator
*/

void inquiry_generate()
{
	cout << "Generating the inquiry data" << endl;
	ofstream inquiryfile;
	inquiryfile.open("inquiry.txt");

	for (int i = 0; i < 6; ++i)
	{
		//product number
		string product_T = product_type[i];

		std::uniform_int_distribution<int> distribution(1,5);
		std::default_random_engine generator(i + 1);
		//price
		vector<string> price = generate_inquiry_price(i + 1, 10);
		for (int j = 1; j <= 10; ++j)
		{
			//index
			int _index = i * 10 + j;
			//side
			string side;
			if (_index % 2 == 0) side = "BUY";
			else side = "SELL";
			
			//quantity
			long quantity = 1000000 * distribution(generator);

			//price
			string temp_price = price[j-1];
			inquiryfile << _index << "," << product_T << "," << side << "," << quantity << "," << temp_price << ",RECEIVED" << endl;
		}
	}
	cout << "Finishing generating the inquiry data" << endl;
}


#endif