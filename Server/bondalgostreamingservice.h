#ifndef BONDALGOSTREAMINGSERVICE_HPP
#define BONDALGOSTREAMINGSERVICE_HPP

/*
	Bond algo streaming service
	Object: BondAlgoExecution
	Derive:
	bondalgostreaming service -> service
	bondalgostreaming servicelistener -> servicelistener
*/

#include <iostream>
#include <unordered_map>
#include <map>
#include "products.hpp"
#include "pricingservice.hpp"
#include "streamingservice.hpp"

// Algo Stream processes algo streaming.
template<typename T>
class AlgoStream
{
private:
	PriceStream<T> stream;
public:
	AlgoStream() {};
	AlgoStream(const PriceStream<T>& _stream);

	const PriceStream<T>& GetStream() const;
};

template<typename T>
AlgoStream<T>::AlgoStream(const PriceStream<T>& _stream)
{
	stream = _stream;
}

template<typename T>
const PriceStream<T>& AlgoStream<T>::GetStream() const
{
	return stream;
}

class BondAlgoStreamingService :public Service< string, AlgoStream<Bond> >
{
private:
	vector< ServiceListener<AlgoStream<Bond> >* > Listeners;
	unordered_map<string, AlgoStream<Bond> > algoStream;
	long count = 0; 
public:
	BondAlgoStreamingService() {};
	
	// Get data on our service given a key
	virtual AlgoStream<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(AlgoStream<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<AlgoStream<Bond>>* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<AlgoStream<Bond>>* >& GetListeners() const override;

	// Generate the price stream and update it to the stored data
	virtual void AddStream(const Price<Bond>& price);
};

AlgoStream<Bond>& BondAlgoStreamingService::GetData(string key)
{
	return algoStream[key];
}

void BondAlgoStreamingService::OnMessage(AlgoStream<Bond>& data)
{
	algoStream[data.GetStream().GetProduct().GetProductId()] = data;
}

void BondAlgoStreamingService::AddListener(ServiceListener<AlgoStream<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<AlgoStream<Bond>>* >& BondAlgoStreamingService::GetListeners() const
{
	return Listeners;
}

void BondAlgoStreamingService::AddStream(const Price<Bond>& price)
{
	double mid = price.GetMid(), spread = price.GetBidOfferSpread();
	long visiblequantity, hiddenquantity;
	if (count % 2 == 0) visiblequantity = 1000000;
	else visiblequantity = 2000000;
	hiddenquantity = 2 * visiblequantity; ++count;

	PriceStreamOrder bid_order(mid - spread / 2, visiblequantity, hiddenquantity, BID);
	PriceStreamOrder ask_order(mid + spread / 2, visiblequantity, hiddenquantity, OFFER);

	PriceStream<Bond> st(price.GetProduct(), bid_order, ask_order);
	AlgoStream<Bond> temp(st);
	algoStream[price.GetProduct().GetProductId()] = temp;

	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(temp);
}

//listener (from pricingservice)
class BondAlgoStreamingServiceListener : public ServiceListener< Price<Bond> >
{
private:
	BondAlgoStreamingService* bondService;
public:
	BondAlgoStreamingServiceListener(BondAlgoStreamingService* BASS);

	// Listener callback to process an add event to the Service
	virtual void ProcessAdd(Price<Bond>& data) override;

	// Listener callback to process a remove event to the Service
	virtual void ProcessRemove(Price<Bond>& data) override;

	// Listener callback to process an update event to the Service
	virtual void ProcessUpdate(Price<Bond>& data) override;
};

BondAlgoStreamingServiceListener::BondAlgoStreamingServiceListener(BondAlgoStreamingService* BASS) :
	bondService(BASS) {}

void BondAlgoStreamingServiceListener::ProcessAdd(Price<Bond>& data)
{
 	bondService->AddStream(data);
}

void BondAlgoStreamingServiceListener::ProcessRemove(Price<Bond>& data) {}
void BondAlgoStreamingServiceListener::ProcessUpdate(Price<Bond>& data) {}

#endif