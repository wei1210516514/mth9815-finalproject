#ifndef BONDGUISERVICE_HPP
#define BONDGUISERVICE_HPP

/*
	Bond GUI service
	Derive:
	bondguiservicelistener -> servicelistener
	bondguiservice -> service
	bondguiconnector -> connector
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include "UtilityFunctions.h"
#include "pricingservice.hpp"
#include "products.hpp" 
#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

class BondGUIConnector;

class BondGUIService : public Service<string, Price<Bond> >
{
private:
	vector<ServiceListener<Price<Bond> >* > Listeners;
	BondGUIConnector* bondguiconnector;
	map<string, Price<Bond> > bondgui;
	long time_point;  //throttles modeling
public:
	BondGUIService();

	// Get data on our service given a key
	virtual Price<Bond>& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(Price<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<Price<Bond>>* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener<Price<Bond>>* >& GetListeners() const override;

	// Get the connector of the service
	BondGUIConnector* GetConnector();

	// Set and Get the millisec of the service
	void SetTimePoint(long& timepoint);
	long GetTimePoint() const;
};

class BondGUIConnector:public Connector< Price<Bond> >
{
private:
	BondGUIService* bondService;
	int count;
public:
	BondGUIConnector(BondGUIService* BGS);

	// Publish data to the Connector
	virtual void Publish(Price<Bond>& data) override;
};


BondGUIConnector::BondGUIConnector(BondGUIService* BGS) :
	bondService(BGS),count(0) {}

void BondGUIConnector::Publish(Price<Bond>& data)
{
	long oldtime = bondService->GetTimePoint();
	long nowMillisec = stoi(CurrentTime(true));
	if (oldtime < nowMillisec) oldtime += 1000;  //at the next second
	if (oldtime - nowMillisec >= 300 && (++count)<=100)  // 300 millisecond throttle
	{ 
		bondService->SetTimePoint(nowMillisec);
		ofstream guifile;
		guifile.open("gui.txt",ios::app);
		
		//Id
		string id = data.GetProduct().GetProductId();
		string bidask = Double2String(data.GetBidOfferSpread());
		string mid_price = Double2String(data.GetMid());
		guifile << CurrentTime(false) << "," << id << "," << mid_price << "," << bidask << endl;
	}
}

BondGUIService::BondGUIService()
{
	bondguiconnector = new BondGUIConnector(this);
	time_point = 0;
}

Price<Bond>& BondGUIService::GetData(string key)
{
	return bondgui[key];
}

void BondGUIService::OnMessage(Price<Bond>& data)
{
	bondgui.insert(pair<string,Price<Bond> >(data.GetProduct().GetProductId(),data));
	bondguiconnector->Publish(data);
}

void BondGUIService::AddListener(ServiceListener<Price<Bond>>* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener<Price<Bond>>* >& BondGUIService::GetListeners() const
{
	return Listeners;
}

BondGUIConnector* BondGUIService::GetConnector()
{
	return bondguiconnector;
}

void BondGUIService::SetTimePoint(long& timepoint)
{
	time_point = timepoint;
}

long BondGUIService::GetTimePoint() const
{
	return time_point;
}

class BondGUIServiceListener : public ServiceListener< Price<Bond> >
{
private:
	BondGUIService* bondService;
public:
	BondGUIServiceListener(BondGUIService* BGS);
	
	// Listener callback to process an add event to the Service
	void ProcessAdd(Price<Bond>& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(Price<Bond>& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(Price<Bond>& data);

};

BondGUIServiceListener::BondGUIServiceListener(BondGUIService* BGS):bondService(BGS){}

void BondGUIServiceListener::ProcessAdd(Price<Bond>& data) 
{
	bondService->OnMessage(data);
}

void BondGUIServiceListener::ProcessRemove(Price<Bond>& data) {}

void BondGUIServiceListener::ProcessUpdate(Price<Bond>& data) {}

#endif
