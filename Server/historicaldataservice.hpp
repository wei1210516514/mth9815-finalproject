/**
 * historicaldataservice.hpp
 * historicaldataservice.hpp
 *
 * @author Breman Thuraisingham
 * Defines the data types and Service for historical data.
 *
 * @author Breman Thuraisingham
 * @update Mingjie Wei
 */
#ifndef HISTORICAL_DATA_SERVICE_HPP
#define HISTORICAL_DATA_SERVICE_HPP

 /*
	 deprecated,see respective historicaldataservice
	 BondPositionService -> HistoricalDataService->positions.txt
	 BondRiskService -> HistoricalDataService->risk.txt
	 BondExecutionService -> HistoricalDataService->executions.txt
	 BondStreamingService -> HistoricalDataService->streaming.txt
	 BondInquiryService -> HistoricalDataService->inquiry.txt
 */

#include <vector>
#include <map>
#include "outputdatageneration.h"
using namespace std;
enum collectType {POSITION,RISK,EXECUTION,STREAMING,INQUIRY};

/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */

template<typename T>
class HistoricalDataService;

template<typename T>
class HistoricalDataConnector: public Connector<T>
{
private:
	HistoricalDataService<T>* bondService;
public:
	// Connector and Destructor
	HistoricalDataConnector(HistoricalDataService<T>* HDS);

	// Publish data to the Connector
	void Publish(T& data);
};

template<typename T>
HistoricalDataConnector<T>::HistoricalDataConnector(HistoricalDataService<T>* HDS) :
	bondService(HDS) {}

template<typename T>
void HistoricalDataConnector<T>::Publish(T& data)
{
	collectType type = bondService->GetCollectType();
	if (type == POSITION) PositionOutput<T>(data);
	else if(type == RISK) RiskOutput<T>(data);
	else if(type == EXECUTION) ExecutionOutput<T>(data);
	else if(type == STREAMING) StreamingOutput<T>(data);
	else if(type == INQUIRY) InquiryOutput<T>(data);
}


template<typename T>
class HistoricalDataService : public Service<string,T>
{
private:
	vector<ServiceListener<T>*> Listeners;
	HistoricalDataConnector<T>* bondTConnector;
	map<string, T> bondT;
	collectType type;
public:
	HistoricalDataService();
	HistoricalDataService(collectType t);

	// Get data on our service given a key
	virtual T& GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(T& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener<T>* listener) override;

	// Get connectors, all listeners and collectType on the Service.
	virtual const vector< ServiceListener<T>* >& GetListeners() const override;

	HistoricalDataConnector<T>* GetConnector() const;
	collectType GetCollectType() const;
    
	// Persist data to a store
    void PersistData(string persistKey, const T& data);
};

template<typename T>
HistoricalDataService<T>::HistoricalDataService() 
{
	bondTConnector = new HistoricalDataConnector<T>(this);
}

template<typename T>
HistoricalDataService<T>::HistoricalDataService(collectType t):type(t)
{
	bondTConnector = new HistoricalDataConnector<T>(this);
}

template<typename T>
T& HistoricalDataService<T>::GetData(string key)
{
	return bondT[key];
}

template<typename T>
void HistoricalDataService<T>::OnMessage(T& data) {}

template<typename T>
void HistoricalDataService<T>::AddListener(ServiceListener<T>* listener)
{
	Listeners.push_back(listener);
}

template<typename T>
const vector< ServiceListener<T>* >& HistoricalDataService<T>::GetListeners() const
{
	return Listeners;
}

template<typename T>
HistoricalDataConnector<T>* HistoricalDataService<T>::GetConnector() const
{
	return bondTConnector;
}

template<typename T>
collectType HistoricalDataService<T>::GetCollectType() const
{
	return type;
}

template<typename T>
void HistoricalDataService<T>::PersistData(string persistKey, const T& data)
{
	bondT[persistKey] = data;
	T temp = data;
	bondTConnector->Publish(temp);
}

//listener from higher-level operations
template<typename T>
class HistoricalDataServiceListener : public ServiceListener<T>
{
private:
	HistoricalDataService<T>* bondService;
public:
	// Connector and Destructor
	HistoricalDataServiceListener(HistoricalDataService<T>* HDSL);

	// Listener callback to process an add event to the Service
	void ProcessAdd(T& data);

	// Listener callback to process a remove event to the Service
	void ProcessRemove(T& data);

	// Listener callback to process an update event to the Service
	void ProcessUpdate(T& data);
};

template<typename T>
HistoricalDataServiceListener<T>::HistoricalDataServiceListener(HistoricalDataService<T>* HDSL) :
	bondService(HDSL) {}

template<typename T>
void HistoricalDataServiceListener<T>::ProcessAdd(T& data)
{
	string _persistKey_ = data.GetProduct().GetProductId();
	bondService->PersistData(_persistKey_, data);
}

template<typename T>
void HistoricalDataServiceListener<T>::ProcessRemove(T& data) {}

template<typename T>
void HistoricalDataServiceListener<T>::ProcessUpdate(T& data) {}

#endif
