#ifndef BONDMARKETDATASERVICE_HPP
#define BONDMARKETDATASERVICE_HPP

/*
	Bond market data service
	Derive:
	marketdata service -> service
	marketdata connector -> connector
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <unordered_map>
#include "marketdataservice.hpp"
#include "products.hpp"
#include "UtilityFunctions.h"

/**
 * Market Data Service which distributes market data
 * Keyed on product identifier.
 * Type T is the product type.
 */
class MarketDataService : public Service<string, OrderBook <Bond> >
{
private:
	map< string, OrderBook<Bond> > MarketData;  //id, orderbook
	vector< ServiceListener< OrderBook<Bond> >* > Listeners;
public:
	MarketDataService() {};
	// Get data on our service given a key
	virtual OrderBook<Bond> & GetData(string key) override;

	// The callback that a Connector should invoke for any new or updated data
	virtual void OnMessage(OrderBook<Bond>& data) override;

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	virtual void AddListener(ServiceListener< OrderBook<Bond> >* listener) override;

	// Get all listeners on the Service.
	virtual const vector< ServiceListener< OrderBook<Bond> >* >& GetListeners() const override;

	// Get the best bid/offer order
	virtual const BidOffer& GetBestBidOffer(const string& productId);

	// Aggregate the order book
	virtual const OrderBook<Bond>& AggregateDepth(const string& productId);
};

OrderBook<Bond>& MarketDataService::GetData(string key)
{
	return MarketData[key];
}

void MarketDataService::OnMessage(OrderBook<Bond>& data)
{
	MarketData[data.GetProduct().GetProductId()] = data;
	for (int i = 0; i < Listeners.size(); ++i)
		Listeners[i]->ProcessAdd(data);
}

void MarketDataService::AddListener(ServiceListener< OrderBook<Bond> >* listener)
{
	Listeners.push_back(listener);
}

const vector< ServiceListener< OrderBook<Bond> >* >& MarketDataService::GetListeners() const
{
	return Listeners;
}

const BidOffer& MarketDataService::GetBestBidOffer(const string& productId)
{
	return MarketData[productId].GetBestBidOffer();
}

const OrderBook<Bond>& MarketDataService::AggregateDepth(const string& productId)
{
	//copy the old and aggregate to a new one
	vector<Order> bidStackOld = MarketData[productId].GetBidStack(),bidStackNew;
	unordered_map<double, long> bidMap;
	for (auto iter = bidStackOld.begin(); iter != bidStackOld.end(); ++iter)
	{
		if(bidMap.find((*iter).GetPrice())!=bidMap.end())
			bidMap[(*iter).GetPrice()] += (*iter).GetQuantity();
		else bidMap[(*iter).GetPrice()] = (*iter).GetQuantity();
	}
	for (auto iter = bidMap.begin(); iter != bidMap.end(); ++iter)
	{
		Order temp(iter->first, iter->second, BID);
		bidStackNew.push_back(temp);
	}

	//copy the old and aggregate to a new one
	vector<Order> askStackOld = MarketData[productId].GetOfferStack(), askStackNew;
	unordered_map<double, long> askMap;
	for (auto iter = askStackOld.begin(); iter != askStackOld.end(); ++iter)
	{
		if (askMap.find((*iter).GetPrice()) != askMap.end())
			askMap[(*iter).GetPrice()] += (*iter).GetQuantity();
		else askMap[(*iter).GetPrice()] = (*iter).GetQuantity();
	}
	for (auto iter = askMap.begin(); iter != askMap.end(); ++iter)
	{
		Order temp(iter->first, iter->second, OFFER);
		askStackNew.push_back(temp);
	}
	OrderBook<Bond> result(MarketData[productId].GetProduct(), bidStackNew, askStackNew);
	return result;
}

//MarketDataConnector to marketdata.txt
class MarketDataConnector : public Connector< OrderBook<Bond> >
{
private:
	MarketDataService* bondService;
public:
	MarketDataConnector(MarketDataService* MDS);

	// Publish data to the Connector
	virtual void Publish(OrderBook<Bond>& data);

	void Subscribe();
};

MarketDataConnector::MarketDataConnector(MarketDataService* MDS):
	bondService(MDS){}

void MarketDataConnector::Publish(OrderBook<Bond>& data) {}

void MarketDataConnector::Subscribe()
{
	boost::asio::io_service io_service;
	//listen for new connection
	tcp::acceptor acceptor_(io_service, tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 4567));
	//socket creation 
	tcp::socket socket_(io_service);
	//waiting for connection
	acceptor_.accept(socket_);

	vector<Order> newBidStack, newOfferStack;
	string newMD;
	int count = 0;
	while (true)
	{
		string tmp = read_(socket_);
		if (tmp == "...") break;
		cout << tmp << endl;
		stringstream flow(tmp);
		vector<string> temp;
		while(getline(flow,tmp,','))
		{
			temp.push_back(tmp);
		}
		string _productID_ = temp[0];
		double bid_price = String2Double(temp[1]);
		double offer_price = String2Double(temp[3]);
		long quantity = stoi(temp[5]);
		Order newBidOrder(bid_price, quantity, BID);
		newBidStack.push_back(newBidOrder);
		Order newOfferOrder(offer_price, quantity, OFFER);
		newOfferStack.push_back(newOfferOrder);
		if (++count == 5)
		{
			OrderBook<Bond> newOrderBook(getBond(_productID_), newBidStack, newOfferStack);
			bondService->OnMessage(newOrderBook);
			newBidStack.clear(); newOfferStack.clear();
			count = 0;
		}
	}
	send_(socket_, "Marketdata data finish transmission!");
}

#endif 