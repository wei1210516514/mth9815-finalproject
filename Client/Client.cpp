#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <boost/asio.hpp>
using namespace std;
using namespace boost::asio;
using ip::tcp;
using std::string;

void read_inquiry_data()
{
	boost::asio::io_service io_service;
	//socket creation
	tcp::socket socket(io_service);
	//connection
	socket.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 1234));

	// request/message from client
	ifstream readdata("inquiry.txt");
	string str;
	boost::system::error_code error;
	while (getline(readdata, str))
	{
		const string msg = str + "\n";
		boost::asio::write(socket, boost::asio::buffer(msg), error);
		if (error) cout << "send failed: " << error.message() << endl;
		Sleep(100);
	}
	const string msg = "...\n";
	boost::asio::write(socket, boost::asio::buffer(msg), error);
	if (error) 	cout << "Client sent hello message!" << endl;

	// getting response from server
	boost::asio::streambuf receive_buffer;
	boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
	if (error && error != boost::asio::error::eof) {
		cout << "receive failed: " << error.message() << endl;
	}
	else {
		const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
		cout << data << endl;
	}
}

void read_price_data()
{
	boost::asio::io_service io_service;
	//socket creation
	tcp::socket socket(io_service);
	//connection
	socket.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 2345));

	// request/message from client
	ifstream readdata("prices.txt");
	string str;
	boost::system::error_code error;
	while (getline(readdata, str))
	{
		const string msg = str + "\n";
		boost::asio::write(socket, boost::asio::buffer(msg), error);
		if (error) cout << "send failed: " << error.message() << endl;
		Sleep(100);
	}
	const string msg = "...\n";
	boost::asio::write(socket, boost::asio::buffer(msg), error);
	if (error) 	cout << "Client sent hello message!" << endl;

	// getting response from server
	boost::asio::streambuf receive_buffer;
	boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
	if (error && error != boost::asio::error::eof) {
		cout << "receive failed: " << error.message() << endl;
	}
	else {
		const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
		cout << data << endl;
	}
}

void read_trade_data()
{
	boost::asio::io_service io_service;
	//socket creation
	tcp::socket socket(io_service);
	//connection
	socket.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 3456));

	// request/message from client
	ifstream readdata("trades.txt");
	string str;
	boost::system::error_code error;
	while (getline(readdata, str))
	{
		const string msg = str + "\n";
		boost::asio::write(socket, boost::asio::buffer(msg), error);
		if (error) cout << "send failed: " << error.message() << endl;
		Sleep(100);
	}
	const string msg = "...\n";
	boost::asio::write(socket, boost::asio::buffer(msg), error);
	if (error) 	cout << "Client sent hello message!" << endl;

	// getting response from server
	boost::asio::streambuf receive_buffer;
	boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
	if (error && error != boost::asio::error::eof) {
		cout << "receive failed: " << error.message() << endl;
	}
	else {
		const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
		cout << data << endl;
	}
}

void read_marketdata_data()
{
	boost::asio::io_service io_service;
	//socket creation
	tcp::socket socket(io_service);
	//connection
	socket.connect(tcp::endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 4567));

	// request/message from client
	ifstream readdata("marketdata.txt");
	string str;
	boost::system::error_code error;
	while (getline(readdata, str))
	{
		const string msg = str + "\n";
		boost::asio::write(socket, boost::asio::buffer(msg), error);
		if (error) cout << "send failed: " << error.message() << endl;
		Sleep(100);
	}
	const string msg = "...\n";
	boost::asio::write(socket, boost::asio::buffer(msg), error);
	if (error) 	cout << "Client sent hello message!" << endl;

	// getting response from server
	boost::asio::streambuf receive_buffer;
	boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
	if (error && error != boost::asio::error::eof) {
		cout << "receive failed: " << error.message() << endl;
	}
	else {
		const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
		cout << data << endl;
	}
}

int main() 
{
	read_trade_data();
	read_marketdata_data();
	read_price_data();
	read_inquiry_data();
	return 0;
}